package org.test4j.tools.commons;

import org.junit.jupiter.api.Test;
import org.test4j.annotations.Mock;
import org.test4j.hamcrest.IWant;
import org.test4j.mock.MockUp;
import org.test4j.tools.IKit;

import java.util.Calendar;
import java.util.Date;

public class DateUtilTest_InlineMockClass implements IWant, IKit {
    @Test
    public void testCurrDateTimeStr_MockUp() throws Exception {
        new MockUp<DateHelper>() {
            @Mock
            public Date now() {
                Calendar cal = DateUtilTest.mockCalendar(2012, 1, 28);
                return cal.getTime();
            }
        };
        String str = DateHelper.currDateTimeStr("MM/dd/yy hh:mm:ss");
        want.string(str).isEqualTo("01/28/12 07:58:55");
    }

    @SuppressWarnings("rawtypes")
    @Test
    public void testCurrDateTimeStr_format() {
        new MockUp(DateHelper.class) {
            @Mock
            public Date now() {
                Calendar cal = DateUtilTest.mockCalendar(2012, 1, 28);
                return cal.getTime();
            }
        };
        String str = DateHelper.currDateTimeStr("MM/dd/yy hh:mm:ss");
        want.string(str).isEqualTo("01/28/12 07:58:55");
    }

    @Test
    public void testCurrDateTimeStr_format_Exception() {
        String str = DateHelper.currDateTimeStr("MM/dd/yy hh:mm:ss");
        want.exception(() ->
                want.string(str).isEqualTo("01/28/12 07:58:55")
            , AssertionError.class);
    }
}