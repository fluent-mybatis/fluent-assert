package org.test4j.tools.commons;

import org.junit.jupiter.api.Test;
import org.test4j.hamcrest.IWant;
import org.test4j.hamcrest.matcher.string.StringMode;

import java.io.File;

public class ResourceHelperTest implements IWant {
    @Test
    public void testIsResourceExists_FileInSamePackage() {
        boolean isExists = ResourceHelper.isResourceExists(ResourceHelper.class, "ResourceHelper.class");
        want.bool(isExists).isEqualTo(true);

        isExists = ResourceHelper.isResourceExists(ResourceHelper.class, "is_utf8.txt");
        want.bool(isExists).isEqualTo(true);
    }

    @Test
    public void testIsResourceExists_FileInSubPackage() {
        boolean isExists = ResourceHelper.isResourceExists(ResourceHelperTest.class, "sub/test.file");
        want.bool(isExists).isEqualTo(true);
    }

    @Test
    public void testIsResourceExists_FileUnexisted() {
        boolean isExists = ResourceHelper.isResourceExists(ResourceHelper.class, "unexists.file");
        want.bool(isExists).isEqualTo(false);
    }

    @Test
    public void testGetSuffixPath() {
        String path = ResourceHelper.getSuffixPath(new File("src/java/org/test4j"), System.getProperty("user.dir")
            + "/src/java/org/test4j/reflector/helper/ClazzHelper.java");
        want.string(path).isEqualTo("reflector/helper/ClazzHelper.java");

        path = ResourceHelper.getSuffixPath(new File("src/java/org/test4j/"), System.getProperty("user.dir")
            + "/src/java/org/test4j/reflector/helper/ClazzHelper.java");

        want.string(path).isEqualTo("reflector/helper/ClazzHelper.java");
    }
}