package org.test4j.tools.commons;

import org.junit.jupiter.api.Test;
import org.test4j.hamcrest.IWant;
import org.test4j.hamcrest.matcher.modes.EqMode;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Set;
import java.util.stream.Collectors;

public class AnnotationHelperTest implements IWant {
    @Test
    public void getFieldsAnnotatedWith() {
        Set<Field> fields = AnnotationHelper.getFieldsAnnotatedWith(Child.class, Demo.class);
        want.list(fields).sizeEq(2);
        want.list(fields.stream().map(Field::getName).collect(Collectors.toList()))
            .eqReflect(new String[]{"field1", "field2"}, EqMode.IGNORE_ORDER);
    }

    @Test
    public void getMethodsAnnotatedWith() {
        Set<Method> methods = AnnotationHelper.getMethodsAnnotatedWith(Child.class, Demo.class);
        want.list(methods).sizeEq(2);
        want.list(methods.stream().map(Method::getName).collect(Collectors.toList()))
            .eqReflect(new String[]{"method1", "method2"}, EqMode.IGNORE_ORDER);
    }

    @Test
    public void getClassLevelAnnotation() {
        Demo demo = AnnotationHelper.getClassLevelAnnotation(Demo.class, Child.class);
        want.object(demo).notNull();
    }

    @Test
    public void getMethodOrClassLevelAnnotationProperty() throws NoSuchMethodException {
        Method method = Child.class.getDeclaredMethod("method2");
        String value = AnnotationHelper.getMethodOrClassLevelAnnotationProperty(Demo.class, "value", "abc", method, Child.class);
        want.string(value).eq("xyz");
    }
}

class Child extends Parent {
    @Demo
    private Integer field2;

    @Demo
    private void method2() {

    }
}

@Demo(value = "xyz")
class Parent {
    @Demo
    private String field1;

    @Demo
    private void method1() {

    }
}

@Target({ElementType.FIELD, ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@interface Demo {
    String value() default "abc";
}