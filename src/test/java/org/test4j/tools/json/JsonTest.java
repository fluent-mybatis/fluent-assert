package org.test4j.tools.json;

import org.junit.jupiter.api.Test;
import org.test4j.Logger;
import org.test4j.tools.IKit;
import org.test4j.tools.commons.DateHelper;

import java.util.Date;

public class JsonTest implements IKit {
    @Test
    public void test_date_json() {
        Date date = DateHelper.parse("2020-04-11 12:04:05");
        String info = json.toJSON(date, true);
        Logger.info(info);
        info = json.toJSON("2020-04-11 12:04:05", true);
        Logger.info(info);
    }
}