package org.test4j.tools;

import org.junit.jupiter.api.Test;
import org.test4j.hamcrest.IWant;

import java.util.Map;

class IKitTest implements IKit, IWant {

    @Test
    void map() {
        Map<String, Object> map = map(m -> m
            .kv("key1", 233)
            .kv("key2", "value2"));
        want.map(map).eqByProperties(arr("key1", "key2"), arr(233, "value2"));
    }
}