package org.test4j.tools.datagen;

import org.junit.jupiter.api.Test;
import org.test4j.hamcrest.IWant;
import org.test4j.tools.IKit;

import java.util.HashMap;
import java.util.List;

class AbstractDataMapTest implements IWant, IKit {

    @Test
    void rows() {
        List list = DataMap.create(2)
            .kv("key1", "value11", "value12")
            .kv("key2", "value21", "value22")
            .rows(HashMap.class);
        want.list(list).eqReflect(DataMap.create(2)
            .kv("key1", "value11", "value12")
            .kv("key2", "value21", "value22")
            .rows()
        );
    }
}