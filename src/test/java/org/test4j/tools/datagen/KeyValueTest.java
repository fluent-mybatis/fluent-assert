package org.test4j.tools.datagen;

import org.junit.jupiter.api.Test;
import org.test4j.hamcrest.IWant;
import org.test4j.hamcrest.matcher.modes.EqMode;
import org.test4j.tools.IKit;

import java.util.List;

@SuppressWarnings("rawtypes")
class KeyValueTest implements IWant, IKit {

    @Test
    void values() {
        MyTestMap map = new MyTestMap(10)
            .testKey.values(1, 2, 6, 7, 9);
        List list = map.cols("test_column");
        want.list(list).eqReflect(new int[]{1, 2, 6, 7, 9, 9, 9, 9, 9, 9});
    }

    @Test
    void testValues() {
        MyTestMap map = new MyTestMap(10)
            .testKey.values(index -> index - 4, 1, 2, 6, 7, 9);
        List list = map.cols("test_column");
        want.list(list).eqReflect(new int[]{1, 1, 1, 1, 1, 2, 6, 7, 9, 9});
    }

    @Test
    void increase() {
        MyTestMap map = new MyTestMap(4)
            .testKey.increase(2, 2);
        List list = map.cols("test_column");
        want.list(list).eqReflect(new int[]{2, 4, 6, 8});
    }

    @Test
    void autoIncrease() {
        MyTestMap map = new MyTestMap(4)
            .testKey.autoIncrease();
        List list = map.cols("test_column");
        want.list(list).eqReflect(new int[]{1, 2, 3, 4});
    }

    @Test
    void formatIncrease() {
        MyTestMap map = new MyTestMap(4)
            .testKey.formatIncrease("name_%d", 1, 1);
        List list = map.cols("test_column");
        want.list(list).eqReflect(new String[]{"name_1", "name_2", "name_3", "name_4"});
    }

    @Test
    void formatAutoIncrease() {
        MyTestMap map = new MyTestMap(4)
            .testKey.formatAutoIncrease("name_%d");
        List list = map.cols("test_column");
        want.list(list).eqReflect(new String[]{"name_1", "name_2", "name_3", "name_4"});
    }

    @Test
    void functionIncrease() {
        MyTestMap map = new MyTestMap(4)
            .testKey.functionIncrease(index -> "name_" + index, 1, 1);
        List list = map.cols("test_column");
        want.list(list).eqReflect(new String[]{"name_1", "name_2", "name_3", "name_4"});
    }

    @Test
    void functionAutoIncrease() {
        MyTestMap map = new MyTestMap(4)
            .testKey.functionAutoIncrease(index -> "name_" + index);
        List list = map.cols("test_column");
        want.list(list).eqReflect(new String[]{"name_1", "name_2", "name_3", "name_4"});
    }

    @Test
    void functionObjs() {
        MyTestMap map = new MyTestMap(4)
            .testKey.functionObjs(obj -> "name_" + obj, 1, 3);
        List list = map.cols("test_column");
        want.list(list).eqReflect(new String[]{"name_1", "name_3", "name_3", "name_3"});
    }

    @Test
    void loop() {
        MyTestMap map = new MyTestMap(4)
            .testKey.loop("name_1", "name_3");
        List list = map.cols("test_column");
        want.list(list).eqReflect(new String[]{"name_1", "name_3", "name_1", "name_3"});
    }

    @Test
    void generateBy() {
        MyTestMap map = new MyTestMap(4)
            .testKey.generateBy((index, arr) -> "name_" + arr[index % 2], 1, 3);
        List list = map.cols("test_column");
        want.list(list).eqReflect(new String[]{"name_1", "name_3", "name_1", "name_3"});
    }

    @Test
    void generate() {
        MyTestMap map = new MyTestMap(4)
            .testKey.generate((index) -> "name_" + index);
        List list = map.cols("test_column");
        want.list(list).eqReflect(new String[]{"name_1", "name_2", "name_3", "name_4"}, EqMode.IGNORE_ORDER);
    }

    @Test
    void random() {
        MyTestMap map = new MyTestMap(4)
            .testKey.random();
        List list = map.cols("test_column");
        want.list(list).sizeEq(4);
    }

    @Test
    void testRandom() {
        MyTestMap map = new MyTestMap(4)
            .testKey.random(1);
        List list = map.cols("test_column");
        want.list(list).eqReflect(new int[]{1, 1, 1, 1});
    }

    @Test
    void testGenerate() {
        DataMap dm = DataMap.create(2)
            .kv("name", "u1", "u2")
            .kv("email", DataGenerator.increase((i, m) -> m.get("name") + "@163.com"));
        want.list(dm.rows()).sizeEq(2)
            .eqByProperties("email", new String[]{"u1@163.com", "u2@163.com"});
    }

    private static class MyTestMap extends AbstractDataMap {
        public MyTestMap(int dataSize) {
            super(dataSize);
        }

        KeyValue<MyTestMap> testKey = new KeyValue<>(this, "test_column", null, () -> true);
    }
}