package org.test4j.tools.reflector;

import org.junit.jupiter.api.Test;
import org.test4j.exception.NoSuchFieldRuntimeException;
import org.test4j.fortest.Address;
import org.test4j.fortest.Employee;
import org.test4j.fortest.ForReflectUtil;
import org.test4j.fortest.User;
import org.test4j.hamcrest.IWant;
import org.test4j.tools.IKit;

import java.util.*;


@SuppressWarnings({"unchecked", "rawtypes"})
public class FieldAccessorTest_FromReflectorUtil implements IWant, IKit {
    @Test
    public void setFieldValue() {
        Employee employee = new Employee();
        want.object(employee.getName()).isNull();
        reflector.setFieldValue(employee, "name", "my name");
        want.object(employee).eqByProperties("name", "my name");
    }

    @Test
    public void setFieldValue_exception() {
        Employee employee = new Employee();
        want.object(employee.getName()).isNull();
        want.exception(() -> reflector.setFieldValue(employee, "name1", "my name")
            , NoSuchFieldRuntimeException.class);
    }

    @Test
    public void setFieldValue_AssertError() {
        want.exception(() -> reflector.setFieldValue(null, "name1", "my name")
            , RuntimeException.class);
    }

    @Test
    public void getFieldValue() {
        Employee employee = new Employee();
        employee.setName("test name");
        Object name = reflector.getFieldValue(employee, "name");
        want.object(name).classIs(String.class);
        want.string(name.toString()).isEqualTo("test name");
    }

    @Test
    public void getFieldValue_exception() {
        try {
            Employee employee = new Employee();
            employee.setName("test name");
            reflector.getFieldValue(employee, "name1");
            want.fail();
        } catch (Throwable e) {
            String message = e.getMessage();
            want.string(message).start("No such field:");
        }
    }

    @Test
    public void getFieldValue_AssertError() {
        want.exception(() -> reflector.getFieldValue(null, "name1")
            , RuntimeException.class);
    }

    @Test
    public void getFieldValue_AssertError2() {
        want.exception(() -> reflector.getFieldValue(Employee.class, "name1")
            , NoSuchFieldRuntimeException.class);
    }

    @Test
    public void testGetArrayItemProperty() {
        List<?> values = ognl.valueList(
            Arrays.asList(new User("ddd", "eeee"), new User("ccc", "dddd")), "first");
        want.list(values).eqReflect(new String[]{"ddd", "ccc"});
    }

    /**
     * 数组类型
     */
    @Test
    public void testGetArrayItemProperty_Array() {
        List<?> values = ognl.valueList(new User[]{new User("ddd", "eeee"),
            new User("ccc", "dddd")}, "first");
        want.list(values).eqReflect(new String[]{"ddd", "ccc"});
    }

    /**
     * 单值
     */
    @Test
    public void testGetArrayItemProperty_SingleValue() {
        List<?> values = ognl.valueList(new User("ddd", "eeee"), "first");
        want.list(values).eqReflect(new String[]{"ddd"});
    }

    /**
     * Map类型
     */
    @Test
    public void testGetArrayItemProperty_Map() {
        Map<String, String> map = new HashMap<>();
        map.put("first", "ddd");

        List<?> values = ognl.valueList(map, "first");
        want.list(values).eqReflect(new String[]{"ddd"});
    }

    /**
     * Map类型_集合
     */
    @Test
    public void testGetArrayItemProperty_MapList() {
        List list = new ArrayList();
        for (int i = 0; i < 2; i++) {
            Map<String, String> map = new HashMap<>();
            map.put("first", "ddd");
            list.add(map);
        }
        List<?> values = ognl.valueList(list, "first");
        want.list(values).eqReflect(new String[]{"ddd", "ddd"});
    }

    @Test
    public void testGetArrayItemProperties() {
        Object[][] values = ognl.valueList(
            Arrays.asList(new User("ddd", "eeee"), new User("ccc", "dddd")), new String[]{"first", "last"});
        want.array(values).eqReflect(new String[][]{{"ddd", "eeee"}, {"ccc", "dddd"}});
    }

    /**
     * 数组类型
     */
    @Test
    public void testGetArrayItemProperties_Array() {
        Object[][] values = ognl.valueList(new User[]{new User("ddd", "eeee"),
            new User("ccc", "dddd")}, new String[]{"first", "last"});
        want.array(values).eqReflect(new String[][]{{"ddd", "eeee"}, {"ccc", "dddd"}});
    }

    /**
     * 单值
     */
    @Test
    public void testGetArrayItemProperties_SingleValue() {
        Object[][] values = ognl.valueList(new User("ddd", "eeee"), new String[]{"first",
            "last"});
        want.array(values).eqReflect(new String[][]{{"ddd", "eeee"}});
    }

    @Test
    // (description = "Map类型")
    public void testGetArrayItemProperties_Map() {
        Map<String, String> map = new HashMap<>();
        map.put("first", "ddd");
        map.put("last", "eeee");

        Object[][] values = ognl.valueList(map, new String[]{"first", "last"});
        want.array(values).eqReflect(new String[][]{{"ddd", "eeee"}});
    }

    @Test
    // (description = "Map类型集合")
    public void testGetArrayItemProperties_MapList() {
        List list = new ArrayList();
        for (int i = 0; i < 2; i++) {
            Map<String, String> map = new HashMap<>();
            map.put("first", "ddd");
            map.put("last", "eeee");
            list.add(map);
        }
        Object[][] values = ognl.valueList(list, new String[]{"first", "last"});
        want.array(values).eqReflect(new String[][]{{"ddd", "eeee"}, {"ddd", "eeee"}});
    }

    @SuppressWarnings("all")
    @Test
    public void testGetProperty() {
        want.exception(() -> ognl.value(null, "dd")
            , RuntimeException.class);
    }

    @Test
    // (description = "对象是Map")
    public void testGetProperty_Map() {
        Map<String, String> map = new HashMap<>();
        map.put("wikiName", "eeee");
        String value = (String) ognl.value(map, "wikiName");
        want.string(value).isEqualTo("eeee");

        want.exception(() -> ognl.value(map, "kkkk"), Exception.class)
            .contains("can't find property[kkkk]");
    }

    @Test
    public void testGetProperty_NoProp() {
        want.exception(() -> ognl.value(item, "dde"), Throwable.class)
            .start("can't find property[dde]");
    }

    ForReflectUtil item = new ForReflectUtil("first name", "last name");

    @Test
    // (description = "Get方法可以取到值")
    public void testGetProperty_GetMethod() {
        String first = (String) ognl.value(item, "first");
        want.string(first).isEqualTo("first name");
    }

    @Test
    // (description = "方法可以取到值_但方法在父类")
    public void testGetProperty_GetMethodInParentClass() {
        SubForReflectUtil item1 = new SubForReflectUtil("first name", "last name");
        String field = (String) ognl.value(item1, "myName");
        want.string(field).isEqualTo("first name,last name");
    }

    @Test
    public void testGetProperty_IsMethod() {
        boolean isMan = (Boolean) ognl.value(item, "man");
        want.bool(isMan).is(true);
    }

    @Test
    // (description = "只能通过直接取字段")
    public void testGetProperty_NotGetMethod() {
        String field = (String) ognl.value(item, "noGetMethod");
        want.string(field).isEqualTo("no get method field");
    }

    @Test
    // (description = "Get方法有逻辑")
    public void testGetProperty_GetMethodHasLogical() {
        String field = (String) ognl.value(item, "myName");
        want.string(field).isEqualTo("first name,last name");
    }

    @Test
    // (description = "只能通过直接取字段_且字段在父类中")
    public void testGetProperty_FieldInParentClass() {
        SubForReflectUtil item1 = new SubForReflectUtil("first name", "last name");
        String field = (String) ognl.value(item1, "noGetMethod");
        want.string(field).isEqualTo("no get method field");
    }

    public static class SubForReflectUtil extends ForReflectUtil {
        public SubForReflectUtil(String first, String last) {
            super(first, last);
        }
    }

    @Test
    // (description = "单值对象_且属性值非集合")
    public void testGetArrayOrItemProperty_SingleValue_And_PropNotList() {
        List values = ognl.valueList(new User("ddd", "ddd"), "first");
        want.list(values).sizeEq(1).eqReflect(new String[]{"ddd"});
    }

    @Test
    // (description = "单值对象_且属性值为集合")
    public void testGetArrayOrItemProperty_SingleValue_PropIsList() {
        User user = new User("ddd", "ddd");
        user.setAddresses(Arrays.asList(new Address("aaa"), new Address("bbb")));
        Collection values = ognl.valueList(user, "addresses");
        want.list(values).sizeEq(2).eqReflect(new Address[]{new Address("aaa"), new Address("bbb")});
    }
}