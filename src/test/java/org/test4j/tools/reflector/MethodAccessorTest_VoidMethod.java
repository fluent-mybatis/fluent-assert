package org.test4j.tools.reflector;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.test4j.fortest.TestObject;
import org.test4j.hamcrest.IWant;
import org.test4j.tools.IKit;

public class MethodAccessorTest_VoidMethod implements IWant, IKit {

    private MethodAccessor throwingMethod;

    @BeforeEach
    public void setUp() {
        throwingMethod = MethodAccessor.method(TestObject.class, "throwingMethod");
    }

    @AfterEach
    public void tearDown() {
        throwingMethod = null;
    }

    /**
     * Test method for
     * .
     *
     * @throws Exception
     */
    @Test
    public void testInvoke() throws Exception {
        Object target = new TestObject();
        MethodAccessor.method(target, "nonThrowingMethod").invoke(target, new Object[0]);
        want.exception(() ->
                throwingMethod.invoke(target, new Object[0])
            , RuntimeException.class);
        want.exception(() ->
                throwingMethod.invoke(target, new Object[]{"wrong"})
            , IllegalArgumentException.class);
    }
}