package org.test4j.tools.reflector;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.test4j.exception.NoSuchFieldRuntimeException;
import org.test4j.fortest.User;
import org.test4j.hamcrest.IWant;
import org.test4j.integration.DataProvider;
import org.test4j.tools.IKit;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@SuppressWarnings({"rawtypes", "unchecked"})
public class PropertyAccessorTest implements IWant, IKit {

    @ParameterizedTest
    @MethodSource("testGetPropertyDatas")
    public void testGetPropertyByOgnl(Object target, String property, String expected) {
        String value = (String) IKit.ognl.value(target, property, true);
        want.string(value).isEqualTo(expected);
    }

    public static Iterator testGetPropertyDatas() {
        return new DataProvider()
            .data(new HashMap() {
                {
                    this.put("key1", "value1");
                    this.put("key2", "value2");
                }
            }, "key1", "value1")
            .data(new HashMap() {
                {
                    this.put("key1", User.mock(12, "darui.wu"));
                    this.put("key2", "value2");
                }
            }, "key1.name", "darui.wu")
            .data(User.mock(12, "darui.wu"), "name", "darui.wu")
            .data(new HashMap() {
                {
                    put("map1.key1", new HashMap() {
                        {
                            put("map2.key2", "ok");
                        }
                    });
                }
            }, "map1.key1.map2.key2", "ok");
    }

    @Test
    public void testGetPropertyByOgnl_NoKey_Failure() {
        Map target = new HashMap() {
            {
                put("map1.key1", new HashMap() {
                    {
                        put("map2.key2", "ok");
                    }
                });
            }
        };
        want.exception(() -> ognl.value(target, "map1.key1.map2.key1", true),
            NoSuchFieldRuntimeException.class);
    }

    @Test
    public void testGetPropertyByOgnl_NoKey_Success() {
        Map target = new HashMap() {
            {
                put("map1.key1", new HashMap() {
                    {
                        put("map2.key2", "ok");
                    }
                });
            }
        };
        Object value = ognl.value(target, "map1.key1.map2.key1", false);
        want.object(value).same(target);
    }
}