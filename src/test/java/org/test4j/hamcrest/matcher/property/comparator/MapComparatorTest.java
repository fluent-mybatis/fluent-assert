package org.test4j.hamcrest.matcher.property.comparator;

import org.junit.jupiter.api.Test;
import org.test4j.hamcrest.IWant;
import org.test4j.hamcrest.fortest.User;
import org.test4j.hamcrest.matcher.modes.EqMode;
import org.test4j.tools.datagen.DataMap;

import java.util.HashMap;

@SuppressWarnings({"rawtypes", "unchecked", "serial"})
public class MapComparatorTest implements IWant {
    @Test
    public void testMap() {
        want.object(new HashMap() {
            {
                this.put("id", 123);
                this.put("name", "darui.wu");
            }
        }).eqReflect(new HashMap() {
            {
                this.put("id", 123);
                this.put("name", null);
            }
        }, EqMode.IGNORE_DEFAULTS);
    }

    @Test
    public void testMap2() {
        want.object(User.mock(123, "darui.wu")).eqReflect(new DataMap() {
            {
                this.kv("id", 123);
                this.kv("name", null);
            }
        }, EqMode.IGNORE_DEFAULTS);
    }

    @Test
    public void testMap3() {
        want.object(new HashMap() {
            {
                this.put("id", 123);
                this.put("name", "darui.wu");
            }
        }).eqReflect(new HashMap() {
            {
                this.put("id", 123L);
                this.put("name", "darui.wu");
            }
        }, EqMode.IGNORE_DEFAULTS);
    }
}