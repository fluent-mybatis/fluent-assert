package org.test4j.hamcrest.matcher.string;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.test4j.hamcrest.IWant;
import org.test4j.integration.DataProvider;

import java.util.Iterator;

@SuppressWarnings("rawtypes")
public class StringNotBlankMatcherTest implements IWant {

    @ParameterizedTest
    @MethodSource("dataForNotBlank")
    public void testNotBlank(String actual) {
        want.exception(() ->
                want.string(actual).notBlank()
            , AssertionError.class);
    }

    public static Iterator dataForNotBlank() {
        return new DataProvider() {
            {
                data((String) null);
                data("");
                data(" ");
                data("\n");
            }
        };
    }

    @Test
    public void testNotBlank_Success() {
        want.string("tt").notBlank();
    }
}