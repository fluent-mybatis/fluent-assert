package org.test4j.hamcrest.base;

import org.junit.jupiter.api.Test;
import org.test4j.hamcrest.IWant;
import org.test4j.hamcrest.iassert.intf.*;

import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;

public class WantStyleAssertionTest implements IWant {
    @Test
    public void wantAssert() {
        want.object(want.string(new String())).classIs(IStringAssert.class);
        want.object(want.bool(true)).classIs(IBooleanAssert.class);
        want.object(want.bool(Boolean.TRUE)).classIs(IBooleanAssert.class);
        // // number
        want.object(want.number(Short.valueOf("1"))).classIs(IShortAssert.class);
        want.object(want.number(1)).classIs(IIntegerAssert.class);
        want.object(want.number(1L)).classIs(ILongAssert.class);
        want.object(want.number(1f)).classIs(IFloatAssert.class);
        want.object(want.number(1d)).classIs(IDoubleAssert.class);
        want.object(want.character('c')).classIs(ICharacterAssert.class);
        want.object(want.bite(Byte.MAX_VALUE)).classIs(IByteAssert.class);

        want.object(want.array(new boolean[]{})).classIs(IArrayAssert.class);
        want.object(want.file(new File(""))).classIs(IFileAssert.class);
    }

    @Test
    public void wantAssert_Failure() {
        want.exception(() ->
                want.fail("error message")
            , AssertionError.class);
    }

    @Test
    public void wantAssert_Failure2() {
        want.exception(() ->
                want.bool(true).isEqualTo(false)
            , AssertionError.class);
    }

    @Test
    public void wantNumber_BigDecimal() {
        want.number(new BigDecimal("100.256")).isEqualTo(new BigDecimal("100.256"));
    }

    @Test
    public void wantNumber_BigInteger() {
        want.number(new BigInteger("10111111111111")).isEqualTo(new BigInteger("10111111111111"));
    }

    @Test
    public void wantNumber_Byte() {
        want.number(new Byte("127")).isEqualTo(new Byte("127"));
    }
}