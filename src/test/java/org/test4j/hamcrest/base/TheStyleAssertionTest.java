package org.test4j.hamcrest.base;

import org.junit.jupiter.api.Test;
import org.test4j.hamcrest.IWant;
import org.test4j.hamcrest.iassert.intf.*;


public class TheStyleAssertionTest implements IWant {
    @Test
    public void theAssert() {
        want.object(the.string()).classIs(IStringAssert.class);
        want.object(the.bool()).classIs(IBooleanAssert.class);
        want.object(the.number()).classIs(INumberAssert.class);
        want.object(the.integer()).classIs(IIntegerAssert.class);
        want.object(the.longnum()).classIs(ILongAssert.class);
        want.object(the.doublenum()).classIs(IDoubleAssert.class);
        want.object(the.floatnum()).classIs(IFloatAssert.class);
        want.object(the.shortnum()).classIs(IShortAssert.class);
        want.object(the.character()).classIs(ICharacterAssert.class);
        want.object(the.bite()).classIs(IByteAssert.class);
        want.object(the.array()).classIs(IArrayAssert.class);
        want.object(the.map()).classIs(IMapAssert.class);
        want.object(the.collection()).classIs(ICollectionAssert.class);
        want.object(the.object()).classIs(IObjectAssert.class);
        want.object(the.file()).classIs(IFileAssert.class);
        want.object(the.calendar()).classIs(IDateAssert.class);
        want.object(the.date()).classIs(IDateAssert.class);
    }

    @Test
    public void testString() {
        want.object(new Integer(1)).eqToString("1");
    }
}