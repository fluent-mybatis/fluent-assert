package org.test4j.hamcrest.diff;

import org.junit.jupiter.api.Test;
import org.test4j.Logger;
import org.test4j.hamcrest.IWant;
import org.test4j.hamcrest.fortest.Address;
import org.test4j.hamcrest.fortest.User;
import org.test4j.tools.commons.ListHelper;
import org.test4j.tools.datagen.DataMap;

/**
 * @author darui.wu
 * @create 2020/4/13 2:20 下午
 */
public class DiffTest_Object implements IWant {
    @Test
    void test_object() {
        DiffMap diff = new DiffFactory().diff(
            new User().setName("name1")
                .setAddress(new Address().setName("add1").setPostcode("123")),
            new User().setName("name1")
                .setAddress(new Address().setName("add1").setPostcode("124"))
        );
        Logger.info(diff.message());
        want.number(diff.diff).isGt(0);
        want.string(diff.message()).contains(new String[]{"$.address.postcode", "(String) 124", "(String) 123"});
    }

    @Test
    void test_object_list() {
        DiffMap diff = new DiffFactory().diff(
            new User().setName("name1")
                .setAddresses(ListHelper.toList(new Address().setName("add1").setPostcode("123"))),
            new User().setName("name1")
                .setAddresses(ListHelper.toList(new Address().setName("add1").setPostcode("124")))
        );
        Logger.info(diff.message());
        want.number(diff.diff).isGt(0);
        want.string(diff.message()).contains(new String[]{"$.addresses[1]~[1].postcode", "(String) 124", "(String) 123"});
    }

    @Test
    void test_diff_by_hybrid() {
        DiffMap diff = new DiffFactory().diff(
            new User().setName("name1")
                .setAddresses(ListHelper.toList(new Address().setName("add1").setPostcode("123"))),
            DataMap.create()
                .kv("name", "name1")
                .kv("addresses", ListHelper.toList(new Address().setName("add1").setPostcode("124")))
        );
        Logger.info(diff.message());
        want.number(diff.diff).isGt(0);
        want.string(diff.message()).contains(new String[]{"$.addresses[1]~[1].postcode", "(String) 124", "(String) 123"});
    }

    @Test
    void test_diff_by_matcher() {
        DiffMap diff = new DiffFactory().diff(
            new User().setName("name1")
                .setAddresses(ListHelper.toList(new Address().setName("add1").setPostcode("123"))),
            DataMap.create()
                .kv("name", "name1")
                .kv("addresses", the.collection().eqReflect(new Object[]{new Address().setName("add1").setPostcode("124")}))
        );
        Logger.info(diff.message());
        want.number(diff.diff).isGt(0);
        want.string(diff.message()).contains(new String[]{"$.addresses", "actual=(ArrayList)", "124", "123"});
    }
}