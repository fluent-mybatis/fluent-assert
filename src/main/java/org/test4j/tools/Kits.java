package org.test4j.tools;

import org.test4j.integration.DataProvider;
import org.test4j.tools.commons.ArrayHelper;
import org.test4j.tools.commons.ListHelper;
import org.test4j.tools.datagen.DataMap;

import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

/**
 * 常用工具方法入口
 *
 * @author wudarui
 */
@SuppressWarnings({"unchecked", "rawtypes"})
public interface Kits {
    /**
     * 构造list对象
     *
     * @param values list元素
     * @return List
     */
    static <T> List<T> list(T... values) {
        return ListHelper.toList(values);
    }

    static <T> T[] toArray(Object o) {
        return (T[]) ArrayHelper.asArray(o);
    }

    /***
     * 构造数组对象
     *
     * @param values object value list
     * @return ignore
     */
    static <T> T[] arr(T... values) {
        return values;
    }

    /**
     * 构造DataProvider数据容器
     *
     * @return DataProvider
     */
    static DataProvider data() {
        return new DataProvider();
    }

    /**
     * 构造map对象
     *
     * @param consumer map构造
     * @return Map
     */
    static <M extends Map> M map(Consumer<DataMap> consumer) {
        DataMap dm = DataMap.create(1);
        consumer.accept(dm);
        return (M) dm.map();
    }
}