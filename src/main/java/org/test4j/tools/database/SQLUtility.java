package org.test4j.tools.database;

import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

import static org.test4j.tools.commons.StringHelper.isSpace;

/**
 * SQL语法高亮显示器
 *
 * @author darui.wu
 */
@SuppressWarnings({"unused"})
public class SQLUtility {

    /**
     * 获取sql语句中的table
     *
     * @param sql sql
     * @return ignore
     */
    public static String parseTable(String sql) {
        return split(sql, SqlKeyWord.Table_Between);
    }

    /**
     * 解析sql语句where之后的语句
     *
     * @param sql sql
     * @return ignore
     */
    public static String parseWhere(String sql) {
        return split(sql, SqlKeyWord.Where_Between);
    }

    /**
     * 解析select 字段列表
     *
     * @param sql sql
     * @return ignore
     */
    public static String[] parseSelect(String sql) {
        String fields = split(sql, SqlKeyWord.Select_Between);
        return Arrays.stream(fields.split(","))
            .map(String::trim).toArray(String[]::new);
    }

    public static String split(String sql, List<SqlKeyWord.ItemBetweenKey> betweenKeys) {
        StringTokenizer tokenizer = new StringTokenizer(sql);
        StringBuilder buff = new StringBuilder();
        boolean hasMatchBegin = false;
        List<String> ends = null;
        while (tokenizer.hasMoreTokens()) {
            String word = tokenizer.nextToken();
            if (!hasMatchBegin) {
                ends = SqlKeyWord.isMatchBegin(betweenKeys, word);
                if (ends != null) {
                    hasMatchBegin = true;
                    continue;
                }
            }
            if (SqlKeyWord.isMatchEnd(ends, word)) {
                break;
            }
            if (hasMatchBegin) {
                buff.append(word).append(" ");
            }
        }
        return buff.toString().trim();
    }

    /**
     * 过滤sql语句中多余的空格
     *
     * @param sql sql
     * @return ignore
     */
    public static String filterSpace(String sql) {
        boolean inSingleQuotation = false;
        boolean inDoubleQuotation = false;
        StringBuilder buff = new StringBuilder();
        char preChar = 0;
        for (char ch : sql.toCharArray()) {
            if (isSpace(ch)) {
                if (inDoubleQuotation) {
                    buff.append(ch);
                } else {
                    buff.append(isSpace(preChar) ? "" : " ");
                }
            } else {
                buff.append(ch);
            }
            if (ch == '"' && preChar != '\\') {
                inDoubleQuotation = !inDoubleQuotation && !inSingleQuotation;
            }
            if (ch == '\'' && preChar != '\\') {
                inSingleQuotation = !inSingleQuotation && !inDoubleQuotation;
            }
            preChar = ch;
        }
        return buff.toString();
    }
}