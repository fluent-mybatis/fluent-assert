package org.test4j.tools.datagen;

import lombok.Getter;
import org.test4j.tools.IKit;
import org.test4j.tools.commons.ArrayHelper;
import org.test4j.tools.datagen.IColData.MulRowValue;
import org.test4j.tools.datagen.IColData.OneRowValue;

import java.util.*;
import java.util.stream.Stream;

/**
 * @author wudarui
 */
@SuppressWarnings({"rawtypes", "unchecked"})
class AbstractDataMap extends LinkedHashMap<String, IColData> implements IDataMap<AbstractDataMap> {

    private static final long serialVersionUID = 1L;

    /**
     * 是否为一个行列式对象
     */
    @Getter
    private final boolean isRowMap;
    /**
     * 记录集大小
     */
    @Getter
    private final int rowSize;

    /**
     * 表示value值非列对象
     */
    AbstractDataMap() {
        this.rowSize = 1;
        this.isRowMap = false;
    }

    /**
     * 表示value值时一个列对象, 并且设置行数 rowSize
     *
     * @param rowSize size
     */
    AbstractDataMap(int rowSize) {
        this.rowSize = rowSize;
        this.isRowMap = true;
    }


    AbstractDataMap(Map<String, Object> map) {
        this();
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            this.kv(entry.getKey(), entry.getValue());
        }
    }

    @Override
    public <M extends Map<String, ?>> M row(int row) {
        if (row < 0) {
            throw new RuntimeException("the index can't less than zero.");
        }
        Map<String, Object> data = new HashMap<>();
        for (Map.Entry<String, IColData> entry : this.entrySet()) {
            data.put(entry.getKey(), entry.getValue().row(row));
        }
        return (M) data;
    }

    @Override
    public AbstractDataMap init(Map map) {
        if (map != null) {
            map.forEach((k, v) -> this.kv(String.valueOf(k), v));
        }
        return this;
    }

    @Override
    public IColData get(String key) {
        return super.get(key);
    }

    @Override
    public Set<String> keySet() {
        return super.keySet();
    }

    @Override
    public List cols(String key) {
        List list = new ArrayList(rowSize);
        for (int row = 0; row < rowSize; row++) {
            list.add(this.get(key).row(row));
        }
        return list;
    }

    @Override
    public AbstractDataMap arr(String key, Object... arr) {
        if (isRowMap) {
            if (arr == null || arr.length == 0) {
                this.putMulRowValue(key, null, new Object[0]);
            } else {
                this.putMulRowValue(key, arr[0], Arrays.copyOfRange(arr, 1, arr.length));
            }
        } else {
            this.putOneRowValue(key, arr, new Object[0]);
        }
        return this;
    }

    @Override
    public AbstractDataMap kv(String key, Object value, Object... values) {
        if (isRowMap) {
            this.putMulRowValue(key, value, values);
        } else {
            this.putOneRowValue(key, value, values);
        }
        return this;
    }

    private void putMulRowValue(String key, Object value, Object[] values) {
        MulRowValue row = new MulRowValue();
        if (value instanceof DataGenerator) {
            ((DataGenerator) value).setDataMap(this);
        }
        row.add(value);
        if (values == null) {
            row.add(null);
            this.put(key, row);
            return;
        }
        /*
         * 第一个对象是否为数组
         */
        boolean isFirstArray = ArrayHelper.isArray(value);
        /*
         * 是否2维数组
         */
        boolean is2dArray = false;
        for (Object item : values) {
            if (ArrayHelper.isArray(item)) {
                is2dArray = true;
                break;
            }
        }
        if (isFirstArray && !is2dArray) {
            row.add(values);
        } else {
            Stream.of(values).forEach(row::add);
        }
        this.put(key, row);
    }

    private void putOneRowValue(String key, Object value, Object[] values) {
        if (values != null && values.length != 0) {
            throw new RuntimeException("this DataMap isn't a RowMap, please use parametrical constructor：new DataMap(size)");
        }
        this.put(key, new OneRowValue(value));
    }

    public void put(String key, Object value) {
        if (value instanceof IColData) {
            if (!isRowMap || value instanceof OneRowValue) {
                this.kv(key, ((IColData) value).row(0));
            } else {
                this.put(key, (IColData) value);
            }
        } else {
            throw new RuntimeException("please use .kv(String, Object, Object ...)");
        }
    }

    @Override
    public List<Map<String, ?>> rows() {
        List<Map<String, ?>> list = new ArrayList<>(rowSize);
        for (int row = 0; row < rowSize; row++) {
            list.add(this.row(row));
        }
        return list;
    }

    @Override
    public <R> List<R> rows(Class<R> klass) {
        return IKit.json.toList(IKit.json.toJSON(rows(), false), klass);
    }

    @Override
    public int getColSize() {
        return this.size();
    }

    @Override
    public boolean containsKey(String key) {
        return super.containsKey(key);
    }

    @Override
    public String toString() {
        if (isRowMap) {
            return IKit.json.toJSON(this.rows(), false);
        } else {
            return IKit.json.toJSON(this.row(1), false);
        }
    }
}