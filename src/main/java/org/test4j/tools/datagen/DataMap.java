package org.test4j.tools.datagen;

import java.util.Map;

@SuppressWarnings({"rawtypes", "unchecked"})
public class DataMap<DM extends DataMap<DM>> extends AbstractDataMap {
    public DataMap() {
    }

    public DataMap(int rowSize) {
        super(rowSize);
    }

    public DataMap(Map<String, Object> map) {
        super(map);
    }

    /**
     * 创建一个普通的Map对象
     *
     * @return ignore
     */
    public static DataMap create() {
        return new DataMap();
    }

    /**
     * 创建一个colSize行的行列式对象
     *
     * @param colSize col size
     * @return ignore
     */
    public static DataMap create(int colSize) {
        return new DataMap(colSize);
    }


    @Override
    public DM arr(String key, Object... arr) {
        return (DM) super.arr(key, arr);
    }

    @Override
    public DM kv(String key, Object value, Object... values) {
        return (DM) super.kv(key, value, values);
    }

    @Override
    public DM init(Map map) {
        return (DM) super.init(map);
    }
}