package org.test4j.tools.datagen;

import lombok.Setter;

import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * 数据生成器
 *
 * @author wudarui
 */
@SuppressWarnings({"rawtypes", "unused"})
public abstract class DataGenerator {
    @Setter
    IDataMap dataMap;

    /**
     * 生成第n个数据<br>
     * index计数从0开始
     *
     * @param index index
     * @return ignore
     */
    public abstract Object generate(int index);

    /**
     * 获取已经设置好字段的对应值
     *
     * @param field field
     * @param row   row index
     * @return ignore
     */
    public Object value(String field, int row) {
        if (this.dataMap == null) {
            throw new RuntimeException("the data map can't be null.");
        }
        if (!this.dataMap.containsKey(field)) {
            throw new RuntimeException("not existed the key[" + field + "] of data map.");
        }
        return this.dataMap.get(field).row(row);
    }

    /**
     * 循环遍历objects对象列表
     *
     * @param objects objects
     * @return ignore
     */
    public static DataGenerator repeat(Object... objects) {
        return new RepeatDataGenerator(objects);
    }

    /**
     * 生成随机对象
     *
     * @param type class
     * @return ignore
     */
    @SuppressWarnings("rawtypes")
    public static DataGenerator random(Class type) {
        return new RandomDataGenerator(type);
    }

    /**
     * 步进生成数据
     *
     * @param from from
     * @param step step
     * @return ignore
     */
    public static DataGenerator increase(Number from, Number step) {
        return new IncreaseDataGenerator(from, step);
    }

    /**
     * 按格式输出
     *
     * @param format 字符串格式， 比如 "abc%sABC",abc是前缀，ABC是后缀，%s是序号占位
     * @param from   序号占位起始值
     * @param step   序号占位增长步长
     * @return ignore
     */
    public static DataGenerator increase(String format, Number from, Number step) {
        return new IncreaseDataGenerator(from, step) {
            @Override
            public Object generate(int index) {
                int _index = (Integer) super.generate(index);
                return String.format(format, _index);
            }
        };
    }

    public static DataGenerator increase(Number from, Number step, Function<Integer, Object> function) {
        return new IncreaseDataGenerator(from, step) {
            @Override
            public Object generate(int index) {
                int _index = (Integer) super.generate(index);
                return function.apply(_index);
            }
        };
    }

    /**
     * 从1开始自增（步长为1）
     *
     * @return ignore
     */
    public static DataGenerator increase() {
        return increase(1, 1);
    }

    /**
     * 从1开始自增（步长为1）
     *
     * @param format 对序号进行格式化处理
     * @return ignore
     */
    public static DataGenerator increase(String format) {
        return increase(format, 1, 1);
    }

    /**
     * 从1开始自增（步长为1）
     *
     * @param function 对序号进行处理的函数
     * @return ignore
     */
    public static DataGenerator increase(Function<Integer, Object> function) {
        return increase(1, 1, function);
    }

    /**
     * 从1开始自增（步长为1）
     *
     * @param function 对序号进行处理的函数
     * @return ignore
     */
    public static DataGenerator increase(BiFunction<Integer, RowData, Object> function) {
        return new DataGenerator() {
            @Override
            public Object generate(int index) {
                return function.apply(index, new RowData(this.dataMap, index));
            }
        };
    }
}