package org.test4j.tools.datagen;

import org.test4j.tools.IKit;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * DataMap：行列式对象
 *
 * @param <DM>
 * @author darui.wu
 */
@SuppressWarnings({"unused", "rawtypes"})
public interface IDataMap<DM extends IDataMap<DM>> {
    /**
     * 往DataMap中赋值
     *
     * @param key    键值
     * @param value  第一个值
     * @param values 后续值
     * @return DataMap
     */
    DM kv(String key, Object value, Object... values);

    /**
     * 往DataMap中赋值
     *
     * @param key key
     * @param arr args
     * @return DataMap
     */
    DM arr(String key, Object... arr);

    /**
     * 使用普通map对象初始化
     *
     * @param map map
     * @return DataMap
     */
    DM init(Map map);

    /**
     * 返回key对应的值
     *
     * @param key key
     * @return IColData
     */
    IColData get(String key);

    /**
     * 返回单个value值时long的场景
     *
     * @param key key
     * @return get long value
     */
    default long getLong(String key) {
        return Long.parseLong(getString(key));
    }

    /**
     * 返回单个value值时布尔值的场景
     *
     * @param key key
     * @return get boolean value
     */
    default boolean getBoolean(String key) {
        return Boolean.parseBoolean(getString(key));
    }

    /**
     * 返回单个value值是String的场景
     *
     * @param key key
     * @return get string value
     */
    default String getString(String key) {
        return String.valueOf(get(key).row(0));
    }


    /**
     * 获取第 row 行对象值
     *
     * @param row index of row
     * @return Map&lt;String, ?&gt;
     */
    <M extends Map<String, ?>> M row(int row);

    /**
     * same as row(0)
     *
     * @return Map
     */
    default <M extends Map<String, ?>> M map() {
        return row(0);
    }

    /**
     * 返回记录集列表
     *
     * @return list map
     */
    <M extends Map> List<M> rows();

    /**
     * 将记录集转换为对象列表
     *
     * @param klass object class
     * @param <R>   object type
     * @return List
     */
    <R> List<R> rows(Class<R> klass);


    /**
     * 返回key列所有的对象值
     *
     * @param key key
     * @return List
     */
    List cols(String key);

    /**
     * 将row行对象json化输出
     *
     * @param row index of row
     * @return The json of nth row value
     */
    default String json(int row) {
        return IKit.json.toJSON(this.row(row), false);
    }

    /**
     * 将第row行对象转换为klass对象
     *
     * @param row   row index
     * @param klass object class
     * @param <R>   object type
     * @return object instance
     */
    default <R> R toObject(int row, Class<R> klass) {
        return IKit.json.toObject(json(row), klass);
    }

    /**
     * 返回记录行数量
     *
     * @return row size
     */
    int getRowSize();

    /**
     * 返回记录列数量
     *
     * @return col size
     */
    int getColSize();

    /**
     * DataMap是否为行列式对象
     *
     * @return true: is row map
     */
    boolean isRowMap();

    /**
     * 返回DataMap的key值列表
     *
     * @return key set
     */
    Set<String> keySet();

    /**
     * 是否包含键值
     *
     * @param key key
     * @return true: contain key
     */
    boolean containsKey(String key);

    Set<Map.Entry<String, IColData>> entrySet();
}