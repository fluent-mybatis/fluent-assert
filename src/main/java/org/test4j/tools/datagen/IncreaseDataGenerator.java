package org.test4j.tools.datagen;

import java.math.BigDecimal;
import java.math.BigInteger;

public class IncreaseDataGenerator extends DataGenerator {
    private final Number from;
    private final Number step;

    public IncreaseDataGenerator(Number from, Number step) {
        this.from = from;
        this.step = step;
    }

    @Override
    public Object generate(int index) {
        if (from instanceof Integer) {
            return from.intValue() + step.intValue() * index;
        }
        if (from instanceof Long) {
            return from.longValue() + step.longValue() * index;
        }
        if (from instanceof Short) {
            Integer value = from.shortValue() + step.shortValue() * index;
            return Short.valueOf(String.valueOf(value));
        }
        if (from instanceof BigInteger) {
            long value = from.longValue() + step.longValue() * index;
            return BigInteger.valueOf(value);
        }
        if (from instanceof Double) {
            return from.doubleValue() + step.doubleValue() * index;
        }
        if (from instanceof Float) {
            return from.floatValue() + step.floatValue() * index;
        }
        if (from instanceof BigDecimal) {
            double value = from.doubleValue() + step.doubleValue() * index;
            return BigDecimal.valueOf(value);
        }

        throw new RuntimeException(
            "not support this type number increase, only support type[int, long, short, double, float, BigInteger, BigDecimal].");
    }
}