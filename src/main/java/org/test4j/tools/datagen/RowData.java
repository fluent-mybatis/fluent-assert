package org.test4j.tools.datagen;

/**
 * RowData: 获取IDataMap单行数据
 *
 * @author darui.wu
 */
@SuppressWarnings({"rawtypes"})
public class RowData {
    private final IDataMap map;
    private final int row;

    RowData(IDataMap map, int row) {
        this.map = map;
        this.row = row;
    }

    public Object get(String key) {
        return map.get(key).row(this.row);
    }
}