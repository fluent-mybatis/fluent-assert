package org.test4j.tools;

import org.test4j.tools.datagen.DataMap;
import org.test4j.tools.json.JSON;
import org.test4j.tools.reflector.PropertyAccessor;
import org.test4j.tools.reflector.Reflector;

import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

/**
 * 基本工具方法入口
 *
 * @author wudarui
 */
@SuppressWarnings({"unchecked", "rawtypes"})
public interface IKit {
    /**
     * 反射工具入口
     */
    Reflector reflector = Reflector.instance;
    /**
     * 属性获取方法
     */
    PropertyAccessor ognl = PropertyAccessor.instance;
    /**
     * json工具方法
     */
    JSON json = JSON.instance;

    /**
     * 构造list对象
     *
     * @param values list元素
     * @return List
     */
    default <T> List<T> list(T... values) {
        return Kits.list(values);
    }

    default <T> T[] toArray(Object o) {
        return Kits.toArray(o);
    }

    /***
     * 构造数组对象
     *
     * @param values object value list
     * @return ignore
     */
    default <T> T[] arr(T... values) {
        return Kits.arr(values);
    }

    /**
     * 构造map对象
     *
     * @param consumer map构造
     * @return Map
     */
    default <M extends Map> M map(Consumer<DataMap> consumer) {
        return Kits.map(consumer);
    }
}