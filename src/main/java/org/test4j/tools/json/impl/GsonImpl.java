package org.test4j.tools.json.impl;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.ToNumberPolicy;
import com.google.gson.reflect.TypeToken;
import org.test4j.tools.commons.StringHelper;
import org.test4j.tools.json.interal.JSONInterface;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * gson实现
 *
 * @author wudarui
 */
@SuppressWarnings({"unchecked"})
public class GsonImpl implements JSONInterface {
    private static final Gson gson = new GsonBuilder()
        .disableHtmlEscaping()
        .setObjectToNumberStrategy(ToNumberPolicy.LONG_OR_DOUBLE)
        .setDateFormat("yyyy-MM-dd HH:mm:ss")
        .create();

    private static final Gson gson_pretty = new GsonBuilder()
        .disableHtmlEscaping()
        .setPrettyPrinting()
        .setObjectToNumberStrategy(ToNumberPolicy.LONG_OR_DOUBLE)
        .setDateFormat("yyyy-MM-dd HH:mm:ss")
        .create();

    @Override
    public String toJSON(Object object, boolean isFormat) {
        if (isFormat) {
            return gson_pretty.toJson(object);
        } else {
            return gson.toJson(object);
        }
    }

    @Override
    public <T> T toObject(String json, Type klass) {
        return gson.fromJson(json, klass);
    }

    @Override
    public <T> T toObject(String json) {
        if (StringHelper.isBlank(json)) {
            return null;
        }
        String text = json.trim();
        if (text.startsWith("[") && text.endsWith("]")) {
            return (T) gson.fromJson(text, HashMap.class);
        } else if (text.startsWith("{") && text.endsWith("}")) {
            return (T) gson.fromJson(text, ArrayList.class);
        } else {
            return (T) json;
        }
    }

    @Override
    public <T> List<T> toList(String json, Class<T> klass) {
        return gson.fromJson(json, TypeToken.getParameterized(ArrayList.class, klass).getType());
    }

    @Override
    public <T> List<T> toList(String json) {
        return gson.fromJson(json, ArrayList.class);
    }
}