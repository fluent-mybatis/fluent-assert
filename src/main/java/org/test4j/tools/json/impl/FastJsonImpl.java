package org.test4j.tools.json.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.test4j.tools.json.interal.JSONInterface;

import java.lang.reflect.Type;
import java.util.List;

import static com.alibaba.fastjson.serializer.SerializerFeature.*;

@SuppressWarnings({"unchecked"})
public class FastJsonImpl implements JSONInterface {
    private static final SerializerFeature[] TO_JSON_NO_FORMAT = {
        WriteDateUseDateFormat,
        NotWriteDefaultValue,
        SkipTransientField,
        IgnoreNonFieldGetter
    };

    private static final SerializerFeature[] TO_JSON_HAS_FORMAT = {
        WriteDateUseDateFormat,
        NotWriteDefaultValue,
        SkipTransientField,
        IgnoreNonFieldGetter,
        PrettyFormat
    };

    @Override
    public String toJSON(Object object, boolean isFormat) {
        if (isFormat) {
            return JSON.toJSONString(object, TO_JSON_HAS_FORMAT);
        } else {
            return JSON.toJSONString(object, TO_JSON_NO_FORMAT);
        }
    }

    @Override
    public <T> T toObject(String json, Type clazz) {
        return JSON.parseObject(json, clazz);
    }

    @Override
    public <T> T toObject(String json) {
        return (T) JSON.parseObject(json);
    }

    @Override
    public <T> List<T> toList(String json, Class<T> aClass) {
        return JSON.parseArray(json, aClass);
    }

    @Override
    public <T> List<T> toList(String json) {
        return (List<T>) JSON.parseArray(json);
    }
}