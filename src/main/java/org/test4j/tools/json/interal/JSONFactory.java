package org.test4j.tools.json.interal;

import org.test4j.tools.config.Config;
import org.test4j.tools.json.impl.FastJsonImpl;
import org.test4j.tools.json.impl.GsonImpl;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * JSONFactory
 *
 * @author wudarui
 */
public class JSONFactory {
    private static JSONInterface instance = null;

    private static final Lock lock = new ReentrantLock();

    public static JSONInterface instance() {
        if (instance == null) {
            lock.lock();
            try {
                if (instance == null) {
                    instance = createJsonInterface();
                }
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }

    private static JSONInterface createJsonInterface() {
        String type = Config.instance().jsonType();
        if ("gson".equalsIgnoreCase(type)) {
            return new GsonImpl();
        } else {
            return new FastJsonImpl();
        }
    }
}