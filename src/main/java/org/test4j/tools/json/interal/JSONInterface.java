package org.test4j.tools.json.interal;

import java.lang.reflect.Type;
import java.util.List;

public interface JSONInterface {
    /**
     * 将对象转换为JSON字符串
     *
     * @param object   object
     * @param isFormat 是否格式化
     * @return ignore
     */
    String toJSON(Object object, boolean isFormat);

    /**
     * 反序列化json串
     *
     * @param json  json string
     * @param klass class
     * @return ignore
     */
    <T> T toObject(String json, Type klass);

    /**
     * 反序列化json串
     *
     * @param json json string
     * @return ignore
     */
    <T> T toObject(String json);

    /**
     * 反序列化json串
     *
     * @param json  json string
     * @param klass class
     * @return ignore
     */
    <T> List<T> toList(String json, Class<T> klass);

    /**
     * 反序列化json串
     *
     * @param json json string
     * @return ignore
     */
    <T> List<T> toList(String json);
}