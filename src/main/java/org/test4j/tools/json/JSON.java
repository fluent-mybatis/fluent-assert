package org.test4j.tools.json;

import org.test4j.tools.json.interal.JSONFactory;

import java.lang.reflect.Type;
import java.util.List;


/**
 * json解码，编码工具类
 *
 * @author darui.wudr
 */
public final class JSON {
    public final static JSON instance = new JSON();

    private JSON() {
    }

    /**
     * 将对象编码为json串
     *
     * @param object   object
     * @param isFormat need format
     * @return ignore
     */
    public String toJSON(Object object, boolean isFormat) {
        return JSONFactory.instance().toJSON(object, isFormat);
    }

    public <T> T toObject(String json, Type klass) {
        return JSONFactory.instance().toObject(json, klass);
    }

    /**
     * 将json字符串反序列为对象
     *
     * @param json json string
     * @return ignore
     */
    public <T> T toObject(String json) {
        return JSONFactory.instance().toObject(json);
    }

    /**
     * 反序列化json串
     *
     * @param json  json string
     * @param klass class
     * @return ignore
     */
    public <T> List<T> toList(String json, Class<T> klass) {
        return JSONFactory.instance().toList(json, klass);
    }

    /**
     * 反序列化json串
     *
     * @param json json string
     * @return ignore
     */
    public <T> List<T> toList(String json) {
        return JSONFactory.instance().toList(json);
    }
}