package org.test4j.tools.reflector;

import lombok.experimental.Accessors;
import org.test4j.Logger;
import org.test4j.tools.commons.ClazzHelper;

import java.lang.reflect.Method;

/**
 * MethodDisplayNameFinder 查找测试方法显示名称
 *
 * @author darui.wu 2019/11/11 5:54 下午
 */
@SuppressWarnings({"unused", "rawtypes", "unchecked"})
public class DisplayNameFinder {

    static String DISPLAY_NAME = "org.junit.jupiter.api.DisplayName";

    static DisplayNameMethod displayNameMethod = null;

    static {
        displayName();
    }

    public static String displayName(Method method) {
        if (method == null) {
            return null;
        }
        if (displayNameMethod != null) {
            Object o = method.getDeclaredAnnotation(displayNameMethod.klass);
            if (o != null) {
                return MethodAccessor.method(displayNameMethod.method).invoke(o);
            }
        }
        return method.getName();
    }


    private static void displayName() {
        try {
            if (ClazzHelper.isClassAvailable(DISPLAY_NAME)) {
                Class clazz = ClazzHelper.getClazz(DISPLAY_NAME);
                displayNameMethod = new DisplayNameMethod(clazz);
            }
        } catch (Exception e) {
            Logger.warn(e.getMessage());
        }
    }

    @Accessors(chain = true)
    static class DisplayNameMethod {
        Class klass;

        Method method;

        public DisplayNameMethod(Class klass) throws Exception {
            this.klass = klass;
            this.method = this.klass.getMethod("value");
        }
    }
}