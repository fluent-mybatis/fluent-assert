package org.test4j.tools.reflector;

import org.test4j.exception.NoSuchFieldRuntimeException;
import org.test4j.exception.NoSuchMethodRuntimeException;
import org.test4j.tools.IKit;
import org.test4j.tools.commons.ClazzHelper;
import org.test4j.tools.commons.PrimitiveHelper;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.*;
import java.util.stream.Collectors;

import static java.lang.reflect.Modifier.isStatic;
import static java.util.Arrays.asList;

@SuppressWarnings({"unchecked", "rawtypes", "unused"})
public class Reflector {
    public final static Reflector instance = new Reflector();

    private Reflector() {
    }

    /**
     * 获取方法
     *
     * @param cls  class
     * @param name field name
     * @return ignore
     */
    public Field getField(Class cls, String name) {
        while (cls != Object.class) {
            try {
                Field field = cls.getDeclaredField(name);
                field.setAccessible(true);
                return field;
            } catch (NoSuchFieldException e) {
                cls = cls.getSuperclass();
            }
        }
        throw new NoSuchFieldRuntimeException("No such field: " + name);
    }

    /**
     * 返回类所有的字段（包括父类的）<br>
     * Gets all fields of the given class and all its super-classes.
     *
     * @param clazz The class
     * @return The fields, not null
     */
    public List<Field> getAllFields(final Class clazz) {
        List<Field> result = new ArrayList<>();
        if (clazz == null || clazz.equals(Object.class)) {
            return result;
        }

        Class type = clazz;
        while (type != Object.class && type != null) {
            // add all fields of this class
            Field[] declaredFields = type.getDeclaredFields();
            result.addAll(asList(declaredFields));

            type = type.getSuperclass();
        }
        return result;
    }

    public void setFieldValue(Object target, String fieldName, Object fieldValue) {
        FieldAccessor.field(target, fieldName).set(target, fieldValue);
    }

    public void setFieldValue(Class klass, String fieldName, Object fieldValue) {
        FieldAccessor.field(klass, fieldName).setStatic(fieldValue);
    }

    public <T> T getFieldValue(Object target, String fieldName) {
        return FieldAccessor.field(target, fieldName).get(target);
    }

    public <T> T getFieldValue(Class klass, String fieldName) {
        return FieldAccessor.field(klass, fieldName).getStatic();
    }

    public <T> T getFieldValue(Object target, Field field) {
        return FieldAccessor.field(field).get(target);
    }

    /**
     * 创建aClass对象实例<br>
     * 不是通过 new Construction()形式
     *
     * @param aClass class
     * @return ignore
     */
    public <T> T newInstance(Class<T> aClass) {
        return ClazzHelper.newInstance(aClass);
    }

    /**
     * 根据构造函数对象
     *
     * @param aClass       class
     * @param argGenerator ConstructorArgsGenerator
     * @return ignore
     */
    public <T> T newInstance(Class<T> aClass, ConstructorArgsGenerator argGenerator) {
        return ClazzHelper.newInstance(aClass, argGenerator);
    }

    /**
     * 从json字符串创建对象
     *
     * @param json json string
     * @return ignore
     */
    public <T> T newInstance(String json) {
        Object o = IKit.json.toObject(json);
        return (T) o;
    }

    /**
     * 从json字符串创建对象
     *
     * @param <T>   class type
     * @param json  json string
     * @param clazz class
     * @return ignore
     */
    public <T> T newInstance(String json, Class<T> clazz) {
        Object o = IKit.json.toObject(json, clazz);
        return (T) o;
    }

    /**
     * 返回class中名称为name,参数类型为 parametersType的方法
     *
     * @param cls            class
     * @param name           method name
     * @param parametersType parameter types
     * @return ignore
     */
    public Method getMethod(Class cls, String name, Class... parametersType) {
        while (cls != Object.class) {
            try {
                Method[] methods = cls.getDeclaredMethods();
                for (Method method : methods) {
                    if (!method.getName().equals(name)) {
                        continue;
                    }
                    if (matchParasType(parametersType, method.getParameterTypes())) {
                        method.setAccessible(true);
                        return method;
                    }
                }
                throw new NoSuchMethodRuntimeException();
            } catch (NoSuchMethodRuntimeException e) {
                cls = cls.getSuperclass();
            }
        }
        throw new NoSuchMethodRuntimeException("No such method: " + name + "(" + Arrays.toString(parametersType) + ")");
    }

    /**
     * 查找名为name，参数个数为args的方法列表
     *
     * @param cls  class
     * @param name method name
     * @param args args size
     * @return ignore
     */
    public List<Method> getMethod(Class cls, String name, int args) {
        List<Method> methods = new ArrayList<>();
        while (cls != Object.class) {
            try {
                Method[] declares = cls.getDeclaredMethods();
                for (Method method : declares) {
                    if (!method.getName().equals(name)) {
                        continue;
                    }
                    if (method.getParameterTypes().length == args) {
                        methods.add(method);
                    }
                }
                throw new NoSuchMethodRuntimeException();
            } catch (NoSuchMethodRuntimeException e) {
                cls = cls.getSuperclass();
            }
        }
        return methods;
    }

    /**
     * Returns all declared setter methods of fields of the given class that are
     * assignable from the given type.
     *
     * @param clazz    The class to get setters from, not null
     * @param type     The type, not null
     * @param isStatic True if static setters are to be returned, false for
     *                 non-static
     * @return A list of Methods, empty list if none found
     */
    public Set<Method> getSettersAssignableFrom(Class clazz, Type type, boolean isStatic) {
        Set<Method> settersAssignableFrom = new HashSet<>();

        Set<Method> allMethods = getAllMethods(clazz);
        for (Method method : allMethods) {
            if (isSetterMethod(method) && ClazzHelper.isAssignable(type, method.getGenericParameterTypes()[0])
                && (isStatic == isStatic(method.getModifiers()))) {
                settersAssignableFrom.add(method);
            }
        }
        return settersAssignableFrom;
    }

    /**
     * Gets all methods of the given class and all its super-classes.
     *
     * @param clazz The class
     * @return The methods, not null
     */
    public Set<Method> getAllMethods(Class clazz) {
        Set<Method> result = new HashSet<>();
        if (clazz == null || clazz.equals(Object.class)) {
            return result;
        }

        // add all methods of this class
        Method[] declaredMethods = clazz.getDeclaredMethods();
        for (Method declaredMethod : declaredMethods) {
            if (declaredMethod.isSynthetic() || declaredMethod.isBridge()) {
                // skip methods that were added by the compiler
                continue;
            }
            result.add(declaredMethod);
        }
        // add all methods of the super-classes
        result.addAll(getAllMethods(clazz.getSuperclass()));
        return result;
    }

    /**
     * Returns the setter methods in the given class that have an argument with
     * the exact given type. The class's superclasses are also investigated.
     *
     * @param clazz The class to get the setter from, not null
     * @param type  The type, not null
     * @return All setters for an object of the given type
     */
    public Set<Method> getSettersOfType(Class clazz, Type type) {
        Set<Method> settersOfType = new HashSet<>();
        Set<Method> allMethods = getAllMethods(clazz);
        for (Method method : allMethods) {
            if (isSetterMethod(method) && method.getGenericParameterTypes()[0].equals(type)) {
                settersOfType.add(method);
            }
        }
        return settersOfType;
    }

    static boolean matchParasType(Class[] expectList, Class[] actualList) {
        if (expectList == null && actualList == null) {
            return true;
        } else if (expectList == null || actualList == null) {
            return false;
        } else if (expectList.length != actualList.length) {
            return false;
        }
        for (int index = 0; index < expectList.length; index++) {
            Class expected = expectList[index];
            Class actual = actualList[index];
            if (expected != null && expected != actual &&
                (actual == null || !actual.isAssignableFrom(expected)) &&
                !PrimitiveHelper.isPrimitiveTypeEquals(expected, actual)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param method The method to check, not null
     * @return True if the given method is the {@link Object#equals} method
     */
    public boolean isEqualsMethod(Method method) {
        return "equals".equals(method.getName()) && 1 == method.getParameterTypes().length
            && Object.class.equals(method.getParameterTypes()[0]);
    }

    /**
     * @param method The method to check, not null
     * @return True if the given method is the {@link Object#hashCode} method
     */
    public boolean isHashCodeMethod(Method method) {
        return "hashCode".equals(method.getName()) && 0 == method.getParameterTypes().length;
    }

    /**
     * @param method The method to check, not null
     * @return True if the given method is the {@link Object#toString} method
     */
    public boolean isToStringMethod(Method method) {
        return "toString".equals(method.getName()) && 0 == method.getParameterTypes().length;
    }

    /**
     * @param method The method to check, not null
     * @return True if the given method is the {@link Object#clone} method
     */
    public boolean isCloneMethod(Method method) {
        return "clone".equals(method.getName()) && 0 == method.getParameterTypes().length;
    }

    public boolean isFinalizeMethod(Method method) {
        return "finalize".equals(method.getName()) && 0 == method.getParameterTypes().length;
    }

    /**
     * 判断方法是否是setter方法<br>
     * For each method, check if it can be a setter for an object of the given
     * type. A setter is a method with the following properties:
     * <ul>
     * <li>Method name is > 3 characters long and starts with set</li>
     * <li>The fourth character is in uppercase</li>
     * <li>The method has one parameter, with the type of the property to set</li>
     * </ul>
     *
     * @param method The method to check, not null
     * @return True if the given method is a setter, false otherwise
     */
    public boolean isSetterMethod(Method method) {
        String methodName = method.getName();
        if (!methodName.startsWith("set") || method.getParameterTypes().length != 1 || methodName.length() < 4) {
            return false;
        }

        String fourthLetter = methodName.substring(3, 4);
        return fourthLetter.toUpperCase().equals(fourthLetter);
    }

    /**
     * 返回所有getter方法
     *
     * @param klass class
     * @return ignore
     */
    public Set<Method> getAllGetterMethod(Class klass) {
        return getAllMethods(klass)
            .stream().filter(this::isGetterMethod)
            .collect(Collectors.toSet());
    }

    /**
     * 是否 getter 方法
     *
     * @param method Method
     * @return ignore
     */
    public boolean isGetterMethod(Method method) {
        Class klass = method.getReturnType();
        if (klass.equals(Void.class)) {
            return false;
        }
        if (method.getParameterCount() != 0) {
            return false;
        }
        if (isStaticMethod(method)) {
            return false;
        }
        String name = method.getName();
        return name.startsWith("get") || name.startsWith("is");
    }

    public Class[] getTypes(Object... args) {
        if (args == null) {
            return new Class[0];
        }

        List<Class> classes = new ArrayList<>();
        for (Object para : args) {
            classes.add(para == null ? null : para.getClass());
        }
        return classes.toArray(new Class[0]);
    }

    /**
     * 是否静态方法
     *
     * @param method Method
     * @return ignore
     */
    public boolean isStaticMethod(Method method) {
        return (method.getModifiers() & Modifier.STATIC) != 0;
    }

    /**
     * 测试类是否是 public static void且是无参形式的?
     *
     * @param method Method
     * @return ignore
     */
    public boolean isPublicStaticVoid(Method method) {
        return method.getReturnType() == void.class && method.getParameterTypes().length == 0
            && (method.getModifiers() & Modifier.STATIC) != 0 && (method.getModifiers() & Modifier.PUBLIC) != 0;
    }

    public <T> T invoke(Object target, String method, Object... args) {
        return MethodAccessor.invoke(target, method, args);
    }

    public <T> T invoke(Class klass, String method, Object... args) {
        return MethodAccessor.invoke(klass, method, args);
    }
}