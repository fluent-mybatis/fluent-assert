package org.test4j.tools.reflector;

import org.test4j.exception.NoSuchFieldRuntimeException;
import org.test4j.tools.commons.ClazzHelper;
import org.test4j.tools.commons.ListHelper;
import org.test4j.tools.commons.StringHelper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static org.test4j.tools.commons.ArrayHelper.isCollOrArray;

/**
 * POJO属性值或Map访问
 *
 * @author darui.wudr
 */
@SuppressWarnings({"unchecked", "rawtypes", "unused"})
public class PropertyAccessor {
    public static final PropertyAccessor instance = new PropertyAccessor();

    private PropertyAccessor() {
    }

    public Object value(final Object target, String ognl) {
        return value(target, ognl, true);
    }

    /**
     * 获得单值对象（非集合或数组,但包含Map）的单个属性值<br>
     * 先访问get方法(is方法)，如果没有get方法再直接访问属性值
     *
     * @param object          object
     * @param ognl            property ognl of object
     * @param throwNoProperty true: if no property, then throw exception
     * @return ignore
     */
    public Object value(final Object object, String ognl, boolean throwNoProperty) {
        String[] expressions = ognl.split("\\.");
        try {
            Object target = object;
            String key = "";
            for (String prop : expressions) {
                if (target == null && !throwNoProperty) {
                    return null;
                } else if (target instanceof Map) {
                    Map map = (Map) target;
                    key = key.equals("") ? prop : key + "." + prop;
                    if (map.containsKey(key)) {
                        target = map.get(key);
                        key = "";
                    }
                } else {
                    target = valueOf(target, prop);
                }
            }
            if (!key.equals("")) {
                throw new NoSuchFieldRuntimeException();
            } else {
                return target;
            }
        } catch (NoSuchFieldRuntimeException e) {
            if (throwNoProperty) {
                String className = object == null ? "null" : object.getClass().getName();
                throw new NoSuchFieldRuntimeException("can't find property[" + ognl + "] in object[" + className + "]",
                    e);
            } else {
                return object;
            }
        }
    }

    public List valueList(Object array, String ognl, boolean throwNoProperty) {
        List result = new ArrayList();
        if (isCollOrArray(array)) {
            Collection list = ListHelper.toList(array);
            for (Object target : list) {
                Object value = value(target, ognl, throwNoProperty);
                result.add(value);
            }
        } else {
            Object value = value(array, ognl, throwNoProperty);
            if (isCollOrArray(value)) {
                result.addAll(ListHelper.toList(value));
            } else {
                result.add(value);
            }
        }
        return result;
    }

    /**
     * 获得数组（集合)中各个对象的属性值列表<br>
     *
     * @param object   数组或集合,如果是单值(或Map)转为size=1的集合处理
     * @param property property of object
     * @return ignore
     */
    public List valueList(Object object, String property) {
        return valueList(object, property, false);
    }

    /**
     * 获得PoJo对象或Map对象的属性值列表
     *
     * @param object   object
     * @param ognlList property ognl of object
     * @return ignore
     */
    public List values(Object object, String[] ognlList, boolean throwException) {
        List<Object> list = new ArrayList<>();
        for (String ognl : ognlList) {
            Object value = value(object, ognl, throwException);
            list.add(value);
        }
        return list;
    }

    /**
     * 获得数组（集合)中各个对象的多个属性值
     *
     * @param arr      数组或集合,如果是单值(或Map)转为size=1的集合处理
     * @param ognlList property of object
     * @return ignore
     */
    public Object[][] valueList(Object arr, String[] ognlList) {
        if (isCollOrArray(arr)) {
            Collection coll = ListHelper.toList(arr);
            List values = new ArrayList();
            for (Object o : coll) {
                Object[] props = values(o, ognlList, false).toArray();
                values.add(props);
            }
            return (Object[][]) values.toArray(new Object[0][0]);
        } else {
            Object[] objects = values(arr, ognlList, false).toArray();
            return new Object[][]{objects};
        }
    }

    /**
     * 获得集合列表中对象的属性值列表
     *
     * @param list            object list
     * @param ognlList        property of object
     * @param throwNoProperty true: if no property, then throw exception
     * @return ignore
     */
    public List<List> valueList(List list, String[] ognlList, boolean throwNoProperty) {
        List<List> result = new ArrayList<>();
        for (Object target : list) {
            List items = new ArrayList();
            if (isCollOrArray(target) && !throwNoProperty) {
                result.add(ListHelper.toList(target));
                continue;
            }
            for (String ognl : ognlList) {
                Object value = value(target, ognl, throwNoProperty);
                items.add(value);
            }
            result.add(items);
        }
        return result;
    }

    /**
     * * o 先根据get方法访问对象的属性，如果存在则返回<br>
     * o 再根据is方法方法对象的属性，且方法值是bool型，返回<br>
     * o 否则，直接方法对象的字段<br>
     * o 如果对象是Map，则根据key值取
     *
     * @param o    object
     * @param prop property of object
     * @return ignore
     */
    private static Object valueOf(final Object o, String prop) {
        if (o == null) {
            throw new RuntimeException("can't get the property value from a null object.");
        }

        if (o instanceof Map) {
            Map map = (Map) o;
            if (map.containsKey(prop)) {
                return map.get(prop);
            } else {
                throw new NoSuchFieldRuntimeException("no key[" + prop + "] value in map.");
            }
        }

        Object target = ClazzHelper.getProxiedObject(o);
        try {
            String method = StringHelper.camel("get", prop);
            return MethodAccessor.invoke(target, method);
        } catch (Throwable e) {
            try {
                String method = StringHelper.camel("is", prop);
                Object result = MethodAccessor.invoke(target, method);
                if (result instanceof Boolean) {
                    return result;
                }
                throw new RuntimeException();
            } catch (Throwable e1) {
                return FieldAccessor.getValue(target, prop);
            }
        }
    }
}