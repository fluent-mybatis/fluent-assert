package org.test4j.tools.reflector;

import java.lang.reflect.Constructor;

/**
 * 构造函数参数值生成器
 *
 * @author darui.wudr
 */
@SuppressWarnings({"rawtypes"})
public interface ConstructorArgsGenerator {
    Object[] generate(Constructor constructor);
}