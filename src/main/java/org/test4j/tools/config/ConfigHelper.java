package org.test4j.tools.config;

import org.test4j.exception.NoSuchFieldRuntimeException;
import org.test4j.tools.commons.StringHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * test4j配置文件工具类
 *
 * @author darui.wudr
 */
@SuppressWarnings({"unused"})
public class ConfigHelper {
    private static Object CONFIG_INSTANCE = null;

    public static Object getConfig() {
        return CONFIG_INSTANCE;
    }

    public static void setConfig(Object config) {
        CONFIG_INSTANCE = config;
    }

    private static final Properties properties = ConfigLoader.loading();

    public static String getString(String key) {
        return properties.getProperty(key);
    }

    /**
     * 获取test4j配置项
     *
     * @param key          key
     * @param defaultValue 默认值
     * @return ignore
     */
    public static String getString(String key, String defaultValue) {
        String value = properties.getProperty(key);
        if (StringHelper.isBlank(value)) {
            return defaultValue;
        } else {
            return value.trim();
        }
    }

    /**
     * 先从properties中获取key的属性值，如果找不到再从test4j配置中查找
     *
     * @param properties Properties
     * @param key        key
     * @return ignore
     */
    public static String getString(Properties properties, String key) {
        String value = getString(properties, key, "");
        if (StringHelper.isBlank(value)) {
            throw new NoSuchFieldRuntimeException("No value found for property " + key);
        } else {
            return value.trim();
        }
    }

    /**
     * Gets the string value for the property with the given name. If no such
     * property is found or the value is empty, the given default value is
     * returned.
     *
     * @param properties   The name, not null
     * @param key          The properties, not null
     * @param defaultValue The default value
     * @return The trimmed string value, not null
     */
    public static String getString(Properties properties, String key, String defaultValue) {
        assert properties != null;
        String value = properties.getProperty(key);
        if (StringHelper.isBlank(value)) {
            value = properties.getProperty(key);
        }
        if (StringHelper.isBlank(value)) {
            return defaultValue;
        } else {
            return value.trim();
        }
    }

    /**
     * Gets the list of comma separated string values for the property with the
     * given name. If no such property is found or the value is empty, an empty
     * list is returned. Empty elements (",,") will not be added. A space
     * (", ,") is not empty, a "" will be added.
     *
     * @param propertyName The name, not null
     * @return The trimmed string list, empty if none found
     */
    public static List<String> getStringList(String propertyName) {
        return getStringList(propertyName, false);
    }

    /**
     * Gets the list of comma separated string values for the property with the
     * given name. If no such property is found or the value is empty, an empty
     * list is returned if not required, else an exception is raised. Empty
     * elements (",,") will not be added. A space (", ,") is not empty, a ""
     * will be added.
     *
     * @param propertyName The name, not null
     * @param required     If true an exception will be raised when the property is
     *                     not found or empty
     * @return The trimmed string list, empty or exception if none found
     */
    public static List<String> getStringList(String propertyName, boolean required) {
        String values = getString(propertyName);
        if (values == null || "".equals(values.trim())) {
            if (required) {
                throw new NoSuchFieldRuntimeException("No value found for property " + propertyName);
            }
            return new ArrayList<>(0);
        }
        String[] splitValues = values.split(",");
        List<String> result = new ArrayList<>(splitValues.length);
        for (String value : splitValues) {
            result.add(value.trim());
        }

        if (required && result.isEmpty()) {
            throw new NoSuchFieldRuntimeException("No value found for property " + propertyName);
        }
        return result;
    }

    public static boolean getBoolean(String key) {
        String prop = properties.getProperty(key);
        return "true".equalsIgnoreCase(prop);
    }

    public static boolean getBoolean(String key, boolean defaultValue) {
        String value = properties.getProperty(key);
        if (StringHelper.isBlank(value)) {
            return defaultValue;
        } else {
            return "true".equalsIgnoreCase(value);
        }
    }

    public static int getInteger(String key, int defaultValue) {
        String prop = properties.getProperty(key);
        try {
            return Integer.parseInt(prop);
        } catch (Throwable e) {
            return defaultValue;
        }
    }

    /**
     * Checks whether the property with the given name exists in the test4j or
     * in the given properties.
     *
     * @param key The property name, not null
     * @return True if the property exists
     */
    public static boolean hasProperty(String key) {
        String value = getString(key);
        return value != null;
    }
}