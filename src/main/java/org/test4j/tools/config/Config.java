package org.test4j.tools.config;


public interface Config {
    Config DEFAULT = new Config() {
    };

    default String jsonType() {
        return ConfigHelper.getString("json.type");
    }

    static Config instance() {
        if (ConfigHelper.getConfig() instanceof Config) {
            return (Config) ConfigHelper.getConfig();
        } else {
            return DEFAULT;
        }
    }
}