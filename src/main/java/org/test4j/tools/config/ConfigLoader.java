package org.test4j.tools.config;

import org.test4j.Logger;
import org.test4j.tools.commons.PropertiesReader;
import org.test4j.tools.commons.StrSubstitutor;

import java.util.Properties;

/**
 * test4j配置文件加载器<br>
 * 默认先加载test4j-default.properties文件中的配置项<br>
 * 根据 {@link #TEST4J_CONFIG_FILES} 加载所有的配置文件<br>
 */
public class ConfigLoader {

    /**
     * Name of the fixed configuration file that contains all defaults
     */
    public static final String DEFAULT_PROPERTIES_FILE_NAME = "test4j-default.properties";

    /**
     * Property in the defaults configuration file that contains the name of the
     * custom configuration file
     */
    public static final String TEST4J_CONFIG_FILES = "test4j.config.files";

    /**
     * reads properties from configuration file
     */
    private final PropertiesReader propertiesReader = new PropertiesReader();

    private static Properties properties = null;

    /**
     * loading the test4j configuration. <br>
     * Include test4j-default.properties, test4j.properties,
     * test4j-local.properties
     *
     * @return the settings, not null
     */
    public static synchronized Properties loading() {
        if (properties == null) {
            ConfigLoader loader = new ConfigLoader();
            properties = new Properties();

            String[] files = loader.loadDefaultConfiguration(properties);
            for (String file : files) {
                loader.loadCustomConfiguration(properties, file);
            }
            loader.loadSystemProperties(properties);
            loader.expandPropertyValues(properties);
        }
        return properties;
    }

    private ConfigLoader() {
    }

    /**
     * Load the default properties file (test4j-default.properties)
     *
     * @param properties The instance to add to loaded properties to, not null
     */
    private String[] loadDefaultConfiguration(Properties properties) {
        Properties defaultProperties = propertiesReader.loadPropertiesFileFromClasspath(DEFAULT_PROPERTIES_FILE_NAME);
        if (defaultProperties == null) {
            throw new RuntimeException("Configuration file: " + DEFAULT_PROPERTIES_FILE_NAME
                + " not found in classpath.");
        }
        properties.putAll(defaultProperties);
        String files = ConfigHelper.getString(defaultProperties, TEST4J_CONFIG_FILES);
        return files.split("[,;]");
    }

    /**
     * Load the custom project level configuration file (test4j.properties)
     *
     * @param properties The instance to add to loaded properties to, not null
     */
    private void loadCustomConfiguration(Properties properties, String file) {
        Properties fileProperties = propertiesReader.loadPropertiesFileFromClasspath(file);
        if (fileProperties == null) {
            Logger.warn("No configuration file " + file + " found.");
        } else {
            properties.putAll(fileProperties);
        }
    }

    /**
     * Load the environment properties.
     *
     * @param properties The instance to add to loaded properties to, not null
     */
    private void loadSystemProperties(Properties properties) {
        properties.putAll(System.getProperties());
    }

    /**
     * Expands all property place holders to actual values.<br>
     * <br>
     * For example suppose you have a property defined as follows:
     * root.dir=/usr/home <br>
     * Expanding following ${root.dir}/subDir will then give following
     * result: /usr/home/subDir
     *
     * @param properties The properties, not null
     */
    private void expandPropertyValues(Properties properties) {
        for (Object key : properties.keySet()) {
            Object value = properties.get(key);
            try {
                String expandedValue = StrSubstitutor.replace(value, properties);
                properties.put(key, expandedValue);
            } catch (Throwable e) {
                throw new RuntimeException("Unable to load configuration. Could not expand property value for key: "
                    + key + ", value " + value, e);
            }
        }

    }
}