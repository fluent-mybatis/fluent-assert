package org.test4j.tools.commons;

import org.test4j.Logger;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Properties;

import static org.test4j.tools.commons.StringHelper.isBlank;

/**
 * PropertiesReader
 *
 * @author wudarui
 */
public class PropertiesReader {
    /**
     * Loads the properties file with the given name, which is available in the
     * classpath. If no file with the given name is found, null is returned.
     *
     * @param propertiesFileName The name of the properties file
     * @return The Properties object, null if the properties file wasn't found.
     */
    public Properties loadPropertiesFileFromClasspath(String propertiesFileName) {
        if (isBlank(propertiesFileName)) {
            throw new IllegalArgumentException("Properties Filename must be given.");
        }
        Properties properties = new Properties();
        try (InputStream inputStream = ResourceHelper.getResourceAsStream(PropertiesReader.class.getClassLoader(), propertiesFileName)) {
            if (inputStream == null) {
                Logger.warn("No configuration file " + propertiesFileName + " found.");
            } else {
                properties.load(inputStream);
            }
            return properties;
        } catch (FileNotFoundException e) {
            Logger.warn("No configuration file " + propertiesFileName + " found.");
            return properties;
        } catch (Throwable e) {
            throw new RuntimeException("Unable to load configuration file: " + propertiesFileName, e);
        }
    }
}