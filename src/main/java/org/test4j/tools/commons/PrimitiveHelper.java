package org.test4j.tools.commons;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.test4j.tools.commons.ListHelper.toList;

/**
 * PrimitiveHelper
 *
 * @author wudarui
 */
@SuppressWarnings({"rawtypes", "unchecked", "unused"})
public class PrimitiveHelper {
    private static final Map<Class, Object> primitiveDefaultValue = new HashMap<>();

    static {
        primitiveDefaultValue.put(String.class, "");
        primitiveDefaultValue.put(Integer.class, 0);
        primitiveDefaultValue.put(Short.class, (short) 0);
        primitiveDefaultValue.put(Long.class, (long) 0);
        primitiveDefaultValue.put(Byte.class, (byte) 0);
        primitiveDefaultValue.put(Float.class, 0.0f);
        primitiveDefaultValue.put(Double.class, 0.0d);
        primitiveDefaultValue.put(Character.class, '\0');
        primitiveDefaultValue.put(Boolean.class, false);

        primitiveDefaultValue.put(int.class, 0);
        primitiveDefaultValue.put(short.class, 0);
        primitiveDefaultValue.put(long.class, 0);
        primitiveDefaultValue.put(byte.class, 0);
        primitiveDefaultValue.put(float.class, 0.0f);
        primitiveDefaultValue.put(double.class, 0.0d);
        primitiveDefaultValue.put(char.class, '\0');
        primitiveDefaultValue.put(boolean.class, false);
    }

    /**
     * 判断2个类型的primitive类型是否一致
     *
     * @param expected expected value
     * @param actual   actual value
     * @return ignore
     */
    public static boolean isPrimitiveTypeEquals(final Class expected, final Class actual) {
        Class _expected = expected;
        if (couples.containsKey(expected)) {
            _expected = couples.get(expected);
        }
        Class _actual = actual;
        if (couples.containsKey(actual)) {
            _actual = couples.get(actual);
        }
        return _expected == _actual;
    }

    private static final Map<Class, Class> couples = new HashMap<>();

    static {
        couples.put(Integer.class, int.class);
        couples.put(Short.class, short.class);
        couples.put(Long.class, long.class);
        couples.put(Byte.class, byte.class);
        couples.put(Float.class, float.class);
        couples.put(Double.class, double.class);
        couples.put(Character.class, char.class);
        couples.put(Boolean.class, boolean.class);
    }

    /**
     * 判断2个数字是否相等
     *
     * @param num_1 number 1
     * @param num_2 number 2
     * @return ignore
     */
    public static boolean doesEqual(Number num_1, Number num_2) {
        if (num_1 == null) {
            return num_2 == null;
        } else if (num_2 == null) {
            return false;
        }
        if (canLongNumber(num_1) && canLongNumber(num_2)) {
            long l1 = num_1.longValue();
            long l2 = num_2.longValue();
            return l1 == l2;
        }
        if (canDoubleNumber(num_1) && canDoubleNumber(num_2)) {
            double d1 = num_1.doubleValue();
            double d2 = num_2.doubleValue();
            return d1 == d2;
        }
        return num_1.equals(num_2);
    }

    /**
     * 是否可以转为Long类型<br>
     * Integer Long Short
     *
     * @param number number
     * @return ignore
     */
    private static boolean canLongNumber(Number number) {
        return number instanceof Long ||
            number instanceof Integer ||
            number instanceof Short ||
            number instanceof BigInteger;
    }

    private static boolean canDoubleNumber(Number number) {
        return number instanceof Double ||
            number instanceof Float ||
            number instanceof BigDecimal;
    }

    private static final Set<Class<?>> primitiveBoxed = new HashSet<>(toList(
        Boolean.class, Byte.class, Short.class, Integer.class, Long.class, Float.class, Double.class, Character.class
    ));

    /**
     * 是否是primitive类型或对应的Object对象
     *
     * @param type class
     * @return ignore
     */
    public static boolean isPrimitiveTypeOrBoxed(Class type) {
        return type.isPrimitive() || primitiveBoxed.contains(type);
    }

    /**
     * 返回有对应primitive类型的默认值
     *
     * @param aClass class
     * @return ignore
     */
    public static Object getPrimitiveDefaultValue(Class aClass) {
        return primitiveDefaultValue.getOrDefault(aClass, null);
    }
}