package org.test4j.tools.commons;

import g_objenesis.org.objenesis.ObjenesisHelper;
import org.springframework.aop.framework.Advised;
import org.test4j.Logger;
import org.test4j.exception.NewInstanceException;
import org.test4j.tools.IKit;
import org.test4j.tools.reflector.ConstructorArgsGenerator;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.*;
import java.util.*;

import static java.lang.reflect.Modifier.isStatic;

@SuppressWarnings({"rawtypes", "unchecked", "unused"})
public class ClazzHelper {
    private static final Map<String, Boolean> clazzAvailableCache = new HashMap<>();

    /**
     * Utility method that verifies whether the class with the given fully
     * qualified classname is available in the classpath.
     *
     * @param className The name of the class
     * @return True if the class with the given name is available
     */
    public static boolean isClassAvailable(String className) {
        Boolean isAvailable = clazzAvailableCache.get(className);
        if (isAvailable != null) {
            return isAvailable;
        }
        try {
            Thread.currentThread().getContextClassLoader().loadClass(className);
            clazzAvailableCache.put(className, true);
            return true;
        } catch (ClassNotFoundException e) {
            clazzAvailableCache.put(className, false);
            return false;
        }
    }

    /**
     * Gets the class for the given name. An Test4JException is thrown when the
     * class could not be loaded.
     *
     * @param className The name of the class, not null
     * @return The class, not null
     */
    public static <T> Class<T> getClazz(String className) {
        try {
            return (Class<T>) Class.forName(className);
        } catch (Throwable t) {
            throw new RuntimeException("Could not load class with name " + className, t);
        }
    }

    public static String getPackFromClassName(String clazzName) {
        int index = clazzName.lastIndexOf(".");
        String pack = "";
        if (index > 0) {
            pack = clazzName.substring(0, index);
        }
        return pack;
    }

    /**
     * 根据类名获得package的路径<br>
     * a.b.c.ImplClazz 返回 a/b/c
     *
     * @param clazzName class name
     * @return class path
     */
    public static String getPathFromPath(String clazzName) {
        String pack = getPackFromClassName(clazzName);
        return pack.replace(".", "/");
    }

    /**
     * 根据类名获得package的路径<br>
     * a.b.c.ImplClazz 返回 "a/b/c"<br>
     * if clazz==null, 返回 ""
     *
     * @param clazz class
     * @return class package path
     */
    public static String getPathFromPath(Class clazz) {
        if (clazz == null) {
            return "";
        } else {
            return getPathFromPath(clazz.getName());
        }
    }

    /**
     * 创建className的实例<br>
     * Creates an instance of the class with the given name. The class's no
     * argument constructor is used to create an instance.
     *
     * @param className The name of the class, not null
     * @return An instance of this class
     * @throws RuntimeException if the class could not be found or no instance
     *                          could be created
     */
    public static <T> T createInstanceOfType(String className) {
        try {
            Class type = Class.forName(className);
            return (T) newInstance(type);
        } catch (ClassCastException e) {
            throw new RuntimeException("Class " + className + " is not of expected type.", e);
        } catch (NoClassDefFoundError e) {
            throw new RuntimeException("Unable to load class " + className, e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Class " + className + " not found", e);
        } catch (RuntimeException e) {
            throw e;
        } catch (Throwable e) {
            throw new RuntimeException("Error while instantiating class " + className, e);
        }
    }

    /**
     * Checks whether the given fromType is assignable to the given toType, also
     * taking into account possible auto-boxing.<br>
     * Check if the right-hand side type may be assigned to the left-hand side
     * type following the Java generics rules.
     *
     * @param fromType The source type, not null
     * @param toType   The to type, not null
     * @return True if assignable
     */
    public static boolean isAssignable(Type fromType, Type toType) {
        if (fromType instanceof Class && toType instanceof Class) {
            Class fromClass = (Class) fromType;
            Class toClass = (Class) toType;

            // handle auto boxing types
            if (boolean.class.equals(fromClass) && Boolean.class.isAssignableFrom(toClass)
                || boolean.class.equals(toClass) && Boolean.class.isAssignableFrom(fromClass)) {
                return true;
            }
            if (char.class.equals(fromClass) && Character.class.isAssignableFrom(toClass) || char.class.equals(toClass)
                && Character.class.isAssignableFrom(fromClass)) {
                return true;
            }
            if (int.class.equals(fromClass) && Integer.class.isAssignableFrom(toClass) || int.class.equals(toClass)
                && Integer.class.isAssignableFrom(fromClass)) {
                return true;
            }
            if (long.class.equals(fromClass) && Long.class.isAssignableFrom(toClass) || long.class.equals(toClass)
                && Long.class.isAssignableFrom(fromClass)) {
                return true;
            }
            if (float.class.equals(fromClass) && Float.class.isAssignableFrom(toClass) || float.class.equals(toClass)
                && Float.class.isAssignableFrom(fromClass)) {
                return true;
            }
            if (double.class.equals(fromClass) && Double.class.isAssignableFrom(toClass)
                || double.class.equals(toClass) && Double.class.isAssignableFrom(fromClass)) {
                return true;
            }
            return toClass.isAssignableFrom(fromClass);
        } else {
            if (toType.equals(fromType)) {
                return true;
            }
            if (toType instanceof ParameterizedType && fromType instanceof ParameterizedType) {
                return isAssignable((ParameterizedType) toType, (ParameterizedType) fromType);
            }
            if (toType instanceof WildcardType) {
                return isAssignable((WildcardType) toType, fromType);
            }
            return false;
        }
    }

    private static boolean isAssignable(ParameterizedType lhsType, ParameterizedType rhsType) {
        if (lhsType.equals(rhsType)) {
            return true;
        }
        Type[] lhsTypeArguments = lhsType.getActualTypeArguments();
        Type[] rhsTypeArguments = rhsType.getActualTypeArguments();
        if (lhsTypeArguments.length != rhsTypeArguments.length) {
            return false;
        }
        for (int size = lhsTypeArguments.length, i = 0; i < size; ++i) {
            Type lhsArg = lhsTypeArguments[i];
            Type rhsArg = rhsTypeArguments[i];
            if (!lhsArg.equals(rhsArg)
                && !(lhsArg instanceof WildcardType && isAssignable((WildcardType) lhsArg, rhsArg))) {
                return false;
            }
        }
        return true;
    }

    private static boolean isAssignable(WildcardType lhsType, Type rhsType) {
        Type[] upperBounds = lhsType.getUpperBounds();
        Type[] lowerBounds = lhsType.getLowerBounds();
        for (int size = upperBounds.length, i = 0; i < size; ++i) {
            if (!isAssignable(upperBounds[i], rhsType)) {
                return false;
            }
        }
        for (int size = lowerBounds.length, i = 0; i < size; ++i) {
            if (!isAssignable(rhsType, lowerBounds[i])) {
                return false;
            }
        }
        return true;
    }

    /**
     * 根据字符串获取枚举值<br>
     * Gets the enum value that has the given name.
     *
     * @param enumClass     The enum class, not null
     * @param enumValueName The name of the enum value, not null
     * @return The actual enum value, not null
     * @throws RuntimeException if no value could be found with the given name
     */
    public static <T extends Enum<?>> T getEnumValue(Class<T> enumClass, String enumValueName) {
        T[] enumValues = enumClass.getEnumConstants();
        for (T enumValue : enumValues) {
            if (enumValueName.equalsIgnoreCase(enumValue.name())) {

                return enumValue;
            }
        }
        throw new RuntimeException("Unable to find a enum value in enum: " + enumClass + ", with value name: "
            + enumValueName);
    }

    /**
     * 读取class的字节码内容byte[]
     *
     * @param clazz class
     * @return class bytes
     */
    public static byte[] getBytes(Class clazz) {
        String name = clazz.getName().replace('.', '/') + ".class";
        try (InputStream iStream = clazz.getClassLoader().getResourceAsStream(name)) {
            ByteArrayOutputStream oStream = new ByteArrayOutputStream();

            assert iStream != null;
            IOUtils.copy(iStream, oStream);
            return oStream.toByteArray();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 创建aClass对象实例<br>
     * 不是通过 new Construction()形式
     *
     * @param aClass class
     * @return object instance
     */
    public static <T> T newInstance(Class<T> aClass) {
        if (aClass.isMemberClass() && !isStatic(aClass.getModifiers())) {
            throw new NewInstanceException(
                "Creation of an instance of a non-static inner class is not possible using reflection. The type "
                    + aClass.getSimpleName()
                    + " is only known in the context of an instance of the enclosing class "
                    + aClass.getEnclosingClass().getSimpleName()
                    + ". Declare the inner class as static to make construction possible.");
        }
        try {
            return innerNewInstance(aClass);
        } catch (NewInstanceException ne) {
            throw ne;
        } catch (Throwable e) {
            throw new NewInstanceException(e);
        }
    }

    private static <T> T innerNewInstance(Class<T> aClass) {
        if (aClass.isInterface()) {
            throw new RuntimeException("create a fake implementation class for interface:" + aClass.getName());
        }
        int modifiers = aClass.getModifiers();
        if (Modifier.isAbstract(modifiers)) {
            throw new RuntimeException("not support abstract class!");
        }

        try {
            Constructor constructor = aClass.getDeclaredConstructor();
            boolean isAccessor = constructor.isAccessible();
            constructor.setAccessible(true);
            Object o = constructor.newInstance();
            constructor.setAccessible(isAccessor);
            return (T) o;
        } catch (Throwable e) {
            return ObjenesisHelper.newInstance(aClass);
        }
    }

    public static <T> T newInstance(Class<T> aClass, ConstructorArgsGenerator argGenerator) {
        Constructor constructor;
        try {
            constructor = aClass.getDeclaredConstructor();
        } catch (Exception e) {
            constructor = aClass.getDeclaredConstructors()[0];
        }
        if (constructor == null) {
            constructor = aClass.getConstructors()[0];
        }

        try {
            Object[] args = argGenerator.generate(constructor);
            boolean isAccessor = constructor.isAccessible();
            constructor.setAccessible(true);
            Object o = constructor.newInstance(args);
            constructor.setAccessible(isAccessor);
            return (T) o;
        } catch (Exception e) {
            Logger.warn("new instance[" + aClass.getName() + "] error.", e);
            return ObjenesisHelper.newInstance(aClass);
        }
    }

    /**
     * 返回可以class中定义的字段(包含父类定义的)<br>
     *
     * @param clazz            class
     * @param filters          需要过滤掉的字段
     * @param includeStatic    是否包含static修饰的字段
     * @param includeFinal     是否包含final修饰的字段
     * @param includeTransient 是否包含transient修饰的字段
     * @return List<Field>
     */
    public static List<Field> getAllFields(Class clazz, Collection<String> filters, boolean includeStatic,
                                           boolean includeFinal, boolean includeTransient) {
        List<Field> jsonFields = new ArrayList<>();

        List<Field> fields = IKit.reflector.getAllFields(clazz);
        for (Field field : fields) {
            String fieldName = field.getName();
            if (filters != null && filters.contains(fieldName)) {
                continue;
            }
            int modifier = field.getModifiers();
            if (!includeStatic && Modifier.isStatic(modifier)) {
                continue;
            }
            if (!includeFinal && Modifier.isFinal(modifier)) {
                continue;
            }
            if (!includeTransient && Modifier.isTransient(modifier)) {
                continue;
            }
            if (jsonFields.contains(field)) {
                continue;
            }
            jsonFields.add(field);
        }
        return jsonFields;
    }

    private final static String Proxy_Type_Pattern = ".*\\$[Pp]roxy\\d+.*";

    /**
     * 返回非代理类的类型
     *
     * @param clazz class
     * @return un proxy class
     */
    public static Class getUnProxyType(Class clazz) {
        if (Proxy.isProxyClass(clazz)) {
            return Object.class;
        }
        Class type = clazz;
        while (type.isAnonymousClass() || type.getName().matches(Proxy_Type_Pattern)) {
            type = type.getSuperclass();
        }

        return type;
    }

    /**
     * 如果是spring代理对象，获得被代理的目标对象
     *
     * @param target object
     * @return proxy target
     */
    public static Object getProxiedObject(Object target) {
        try {
            if (ClazzHelper.isClassAvailable("org.springframework.aop.framework.Advised")) {
                return getAdvisedObject(target);
            } else {
                return target;
            }
        } catch (Exception e) {
            Logger.warn("get proxy object error.", e);
            return target;
        }
    }

    /**
     * 返回spring代理的目标对象
     *
     * @param target object
     * @return ignore
     */
    public static Object getAdvisedObject(Object target) {
        if (!ClazzHelper.isClassAvailable("org.springframework.aop.framework.Advised")) {
            return target;
        }
        if (target instanceof Advised) {
            try {
                return ((Advised) target).getTargetSource().getTarget();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } else {
            return target;
        }
    }
}