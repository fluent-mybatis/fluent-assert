package org.test4j.tools.commons;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * ResourceHelper
 *
 * @author wudarui
 */
@SuppressWarnings({"rawtypes", "unused"})
public class ResourceHelper {

    /**
     * To convert the InputStream to String we use the BufferedReader.readLine()
     * method. We iterate until the BufferedReader return null which means
     * there's no more data to read. Each line will appended to a StringBuilder
     * and returned as String.
     *
     * @param is       InputStream
     * @param encoding 编码格式
     * @return ignore
     */
    public static String readFromStream(InputStream is, String encoding) {
        String line;
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(is, encoding))) {
            StringBuilder buffer = new StringBuilder();
            boolean isFirst = true;
            while ((line = reader.readLine()) != null) {
                if (isFirst) {
                    isFirst = false;
                } else {
                    buffer.append("\n");
                }
                buffer.append(line);
            }
            return buffer.toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * To convert the InputStream to String we use the BufferedReader.readLine()
     * method. We iterate until the BufferedReader return null which means
     * there's no more data to read. Each line will appended to a StringBuilder
     * and returned as String.
     *
     * @param is InputStream
     * @return ignore
     */
    public static String readFromStream(InputStream is) {
        return readFromStream(is, "utf8");
    }

    /**
     * 从文件流中读取文本行
     *
     * @param is       InputStream
     * @param encoding 编码格式
     * @return 返回文本行列表
     */
    public static String[] readLinesFromStream(InputStream is, String encoding) {
        String line;
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(is, encoding))) {
            List<String> list = new ArrayList<>();
            while ((line = reader.readLine()) != null) {
                list.add(line);
            }
            return list.toArray(new String[0]);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 从文件流中读取文本行
     *
     * @param is InputStream
     * @return 返回文本行列表
     */
    public static String[] readLinesFromStream(InputStream is) {
        return readLinesFromStream(is, "utf8");
    }

    /**
     * 写字符串到文件中
     *
     * @param file    File
     * @param content text
     */
    public static void writeStringToFile(File file, String content) {
        mkFileParentDir(file);
        try (FileWriter writer = new FileWriter(file, false)) {
            writer.write(content);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 把classpath中的文件内容复制到指定的外部文件中
     *
     * @param classPathResourceName  classpath下资源文件
     * @param fileSystemResourceName 拷贝目标的系统文件名称
     */
    public static void copyClassPathResource(String classPathResourceName, String fileSystemResourceName) {
        copyClassPathResource(classPathResourceName, new File(fileSystemResourceName));
    }

    /**
     * 把classpath中的文件内容复制到指定的外部文件中
     *
     * @param classPathResourceName  classpath下资源文件
     * @param fileSystemResourceFile 拷贝目标的系统文件
     */
    public static void copyClassPathResource(String classPathResourceName, File fileSystemResourceFile) {
        mkFileParentDir(fileSystemResourceFile);
        ClassLoader loader = ResourceHelper.class.getClassLoader();
        try (InputStream resourceInputStream = loader.getResourceAsStream(classPathResourceName);
             OutputStream fileOutputStream = new FileOutputStream(fileSystemResourceFile)) {
            assert resourceInputStream != null;
            IOUtils.copy(resourceInputStream, fileOutputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 文件缺省的编码格式
     *
     * @return ignore
     */
    public static String defaultFileEncoding() {
        return System.getProperty("file.encoding", "utf-8");
    }

    /**
     * 根据文件名称返回文件输入流<br>
     * <br>
     * o file:开头读取文件系统 <br>
     * o classpath:开头读取classpath下的文件<br>
     * o 否则读取 classpath下文件
     *
     * @param klassLoader ClassLoader
     * @param file        File
     * @return ignore
     * @throws FileNotFoundException FileNotFoundException
     */
    public static InputStream getResourceAsStream(ClassLoader klassLoader, final String file) throws FileNotFoundException {
        String _file = file;
        if (file == null) {
            throw new RuntimeException("execute file name can't be null!");
        }
        if (file.startsWith("file:")) {
            _file = file.replaceFirst("file:", "");
            return new FileInputStream(_file);
        } else {
            if (file.startsWith("classpath:")) {
                _file = file.replaceFirst("classpath:", "");
            }
            return klassLoader.getResourceAsStream(_file);
        }
    }

    /**
     * 返回文件的URL<br>
     * <br>
     * o file:开头读取文件系统 <br>
     * o classpath:开头读取classpath下的文件<br>
     * o 否则读取 classpath下文件
     *
     * @param filename file name
     * @return ignore
     */
    @SuppressWarnings("deprecation")
    public static URL getResourceUrl(final String filename) {
        if (filename == null) {
            throw new RuntimeException("execute file name can't be null!");
        }
        if (filename.startsWith("file:")) {
            try {
                String _filename = filename.replaceFirst("file:", "");
                File file = new File(_filename);
                return file.toURL();
            } catch (MalformedURLException e) {
                throw new RuntimeException(e);
            }
        } else {
            String resource = filename;
            if (filename.startsWith("classpath:")) {
                resource = filename.replaceFirst("classpath:", "");
            }
            return ResourceHelper.class.getClassLoader().getResource(resource);
        }
    }

    public static String readFromClasspath(String file) {
        try (InputStream is = ResourceHelper.class.getClassLoader().getResourceAsStream(file)) {
            return readFromStream(is);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 先读取claz所在package下的url资源<br>
     * 如果没有找到，在读取跟classpath下的url资源
     *
     * @param clazz    Class
     * @param fileName file name
     * @return ignore
     * @throws FileNotFoundException FileNotFoundException
     */
    public static InputStream getResourceAsStream(Class clazz, String fileName) throws FileNotFoundException {
        String packPath = ClazzHelper.getPathFromPath(clazz);
        String url = StringHelper.isBlank(packPath) ? fileName : packPath + "/" + fileName;
        InputStream is = ResourceHelper.class.getClassLoader().getResourceAsStream(url);

        if (is == null && !StringHelper.isBlank(packPath)) {
            is = clazz.getClassLoader().getResourceAsStream(fileName);
        }

        if (is == null) {
            if (StringHelper.isBlank(packPath)) {
                throw new FileNotFoundException(String.format("can't find class path resource in in classpath: [%s].",
                    fileName));
            } else {
                throw new FileNotFoundException(String.format(
                    "can't find class path resource in in classpaths: [%s] and [%s]", url, fileName));
            }
        } else {
            return is;
        }
    }

    /**
     * 从文本文件中读取内容 <br>
     * <br>
     * o file:开头读取文件系统 <br>
     * o classpath:开头读取classpath下的文件<br>
     * o 否则读取跟 classpath下filePath文件
     *
     * @param klassLoader ClassLoader
     * @param file        file
     * @return ignore
     */
    public static String readFromFile(ClassLoader klassLoader, String file) {
        try {
            try (InputStream stream = getResourceAsStream(klassLoader, file)) {
                return readFromStream(stream);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 从文本文件中读取内容 <br>
     * <br>
     * o file:开头读取文件系统 <br>
     * o classpath:开头读取classpath下的文件<br>
     * o 否则读取跟 classpath下filePath文件
     *
     * @param file file path
     * @return ignore
     */
    public static String readFromFile(String file) {
        try {
            try (InputStream stream = getResourceAsStream(ResourceHelper.class.getClassLoader(), file)) {
                return readFromStream(stream);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String[] readLinesFromFile(File file) {
        try {
            try (InputStream stream = new FileInputStream(file)) {
                return readLinesFromStream(stream);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 从文本文件中读取内容
     *
     * @param file File
     * @return ignore
     * @throws FileNotFoundException FileNotFoundException
     */
    public static String readFromFile(File file) throws FileNotFoundException {
        if (file == null) {
            throw new RuntimeException("the file can't be null.");
        } else if (!file.exists()) {
            throw new RuntimeException(String.format("the file[%s] isn't exists.", file.getName()));
        }

        return readFromStream(new FileInputStream(file), "utf8");
    }

    /**
     * 读取文件内容<br>
     * * <br>
     * o file:开头读取文件系统 <br>
     * o classpath:开头读取classpath下的文件<br>
     * o 否则, 先读取clazz path下的fileName文件,再读取跟路径下的fileName文件
     *
     * @param clazz    文件所在的class package
     * @param fileName file name
     * @return ignore
     */
    public static String readFromFile(Class clazz, String fileName) {
        if (StringHelper.isBlank(fileName)) {
            throw new RuntimeException("file name can't be null.");
        }
        if (fileName.startsWith("file:") || fileName.startsWith("classpath:")) {
            return readFromFile(clazz.getClassLoader(), fileName);
        } else {
            try {
                try (InputStream is = getResourceAsStream(clazz, fileName)) {
                    return readFromStream(is);
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * 按行读取文件
     *
     * @param filePath file path
     * @return ignore
     */
    public static String[] readLinesFromFile(ClassLoader klassLoader, String filePath) {
        try {
            try (InputStream stream = getResourceAsStream(klassLoader, filePath)) {
                return readLinesFromStream(stream);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String[] readLinesFromFile(ClassLoader klassLoader, String filePath, String encoding) {
        try {
            try (InputStream stream = getResourceAsStream(klassLoader, filePath)) {
                return readLinesFromStream(stream, encoding);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 判断在aClass对应的package下面是否存在名称为file的文件
     *
     * @param aClass class
     * @param file   file in class package path
     * @return ignore
     */
    public static boolean isResourceExists(Class aClass, String file) {
        String pack = ClazzHelper.getPathFromPath(aClass);
        String path = StringHelper.isBlank(pack) ? file : pack + "/" + file;
        try {
            URL url = ClassLoader.getSystemResource(path);
            return url != null;
        } catch (Throwable e) {
            return false;
        }
    }

    /**
     * 获得javaFile相对于basePath的子路径<br>
     * 例如<br>
     * filePath = src/java/org/test4j/reflector/helper/ClazzHelper.java<br>
     * basepath = src/java/org/test4j<br>
     * 那么返回值就为 reflector/helper/ClazzHelper.java
     *
     * @param basePath 跟路径
     * @param filePath 文件绝对路径名称
     * @return ignore
     */
    public static String getSuffixPath(File basePath, String filePath) {
        String prefixPath = basePath.getAbsolutePath().replaceAll("[/\\\\]+", "/");
        String fullPath = filePath.replaceAll("[/\\\\]+", "/");

        String suffixPath = filePath;
        if (fullPath.startsWith(prefixPath)) {
            suffixPath = fullPath.replace(prefixPath, "");
        }
        if (suffixPath.startsWith("/")) {
            suffixPath = suffixPath.substring(1);
        }
        return suffixPath;
    }

    /**
     * 删除文件或目录
     *
     * @param path file path
     * @return ignore
     */
    public static boolean deleteFileOrDir(String path) {
        try {
            return deleteFileOrDir(new File(path));
        } catch (Throwable e) {
            return false;
        }
    }

    /**
     * 删除文件或目录
     *
     * @param path file path
     * @return ignore
     */
    public static boolean deleteFileOrDir(File path) {
        if (!path.exists()) {
            return true;
        }

        if (path.isFile()) {
            return path.delete();
        }
        File[] files = path.listFiles();
        assert files != null;
        for (File file : files) {
            if (file.isDirectory()) {
                deleteFileOrDir(file);
            } else {
                file.delete();
            }
        }
        return (path.delete());
    }

    /**
     * 创建文件file所在的父目录
     *
     * @param file file
     */
    public static void mkFileParentDir(File file) {
        File path = file.getParentFile();
        if (!path.exists()) {
            path.mkdirs();
        }
    }
}