package org.test4j.tools.generic;

import java.lang.reflect.TypeVariable;

/**
 * 泛型声明变量定位
 *
 * @author darui.wudr 2013-10-30 上午11:28:54
 */
@SuppressWarnings("rawtypes")
public class GenericTypeKey {
    private final String className;

    private final String simpleClassName;

    private final String genericName;

    public GenericTypeKey(Class aClass, String genericName) {
        this.className = aClass.getName();
        this.simpleClassName = aClass.getSimpleName();
        this.genericName = genericName;
    }

    public GenericTypeKey(String aClass, String genericName) {
        this.className = aClass;
        this.simpleClassName = aClass.substring(aClass.lastIndexOf('.') + 1);
        this.genericName = genericName;
    }

    public GenericTypeKey(TypeVariable typeVariable) {
        this((Class) typeVariable.getGenericDeclaration(), typeVariable.getName());
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((className == null) ? 0 : className.hashCode());
        result = prime * result + ((genericName == null) ? 0 : genericName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        GenericTypeKey other = (GenericTypeKey) obj;
        if (className == null) {
            if (other.className != null)
                return false;
        } else if (!className.equals(other.className))
            return false;
        if (genericName == null) {
            return other.genericName == null;
        } else {
            return genericName.equals(other.genericName);
        }
    }

    @Override
    public String toString() {
        return simpleClassName + "<" + genericName + ">";
    }
}