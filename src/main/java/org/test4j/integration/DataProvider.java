package org.test4j.integration;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

/**
 * 数据迭代器
 *
 * @author wudarui
 */
public class DataProvider implements Iterator<Object[]> {
    private final List<Object[]> data = new ArrayList<>();
    private Iterator<Object[]> it = null;

    /**
     * 增加一行数据
     *
     * @param data list
     * @return DataProvider
     */
    public DataProvider data(Object... data) {
        this.checkDataLength(data);
        this.data.add(data);
        this.index++;
        return this;
    }

    @Override
    public boolean hasNext() {
        this.initIterator();
        return it.hasNext();
    }

    @Override
    public Object[] next() {
        this.initIterator();
        return it.next();
    }

    @Override
    public void remove() {
        this.initIterator();
        it.remove();
    }

    private final Lock lock = new ReentrantLock();

    private void initIterator() {
        if (it == null) {
            try {
                lock.lock();
                if (it == null) {
                    it = this.data.iterator();
                }
            } finally {
                lock.unlock();
            }
        }
    }

    private int index = 1;
    private int prev = 0;

    /**
     * 检查数据长度是否一致
     *
     * @param data list
     */
    private void checkDataLength(Object... data) {
        int length = data.length;
        if (length == 0) {
            throw new RuntimeException(String.format("provider data(index %d) error, can't be empty.", index));
        }
        if (prev == 0 || prev == length) {
            prev = length;
        } else {
            String str = Arrays.stream(data).map(String::valueOf).collect(Collectors.joining(", ", "[", "]"));
            String ERROR_MSG = "DataProvider error, the previous data length is %d, but current data(data index %d) %s length is %d.";
            String error = String.format(ERROR_MSG, prev, index, str, length);
            throw new RuntimeException(error);
        }
    }
}
