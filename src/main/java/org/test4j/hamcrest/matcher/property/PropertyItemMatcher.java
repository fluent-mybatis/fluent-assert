package org.test4j.hamcrest.matcher.property;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.test4j.tools.IKit;
import org.test4j.tools.commons.ArrayHelper;
import org.test4j.tools.commons.ListHelper;

import java.util.List;


/**
 * 属性值集合或属性值作为一个对象，要满足指定的断言
 *
 * @author darui.wudr
 */
@SuppressWarnings("rawtypes")
public class PropertyItemMatcher extends BaseMatcher {
    private final String property;
    private final Matcher matcher;

    public PropertyItemMatcher(String property, Matcher matcher) {
        this.property = property;
        this.matcher = matcher;
        if (this.property == null) {
            throw new RuntimeException("the property can't be null.");
        }
    }

    @Override
    public boolean matches(Object actual) {
        if (actual == null) {
            buff.append("properties equals matcher, the actual value can't be null.");
            return false;
        }
        if (ArrayHelper.isCollOrArray(actual)) {
            List list = ListHelper.toList(actual);
            List actualList = IKit.ognl.valueList(list, property, true);
            return this.matcher.matches(actualList);
        } else {
            Object actualValue = IKit.ognl.value(actual, property, true);
            return this.matcher.matches(actualValue);
        }
    }

    private final StringBuilder buff = new StringBuilder();

    @Override
    public void describeTo(Description description) {
        description.appendText("the property[" + this.property + "] of object must match");
        description.appendText(buff.toString());

        matcher.describeTo(description);
    }
}