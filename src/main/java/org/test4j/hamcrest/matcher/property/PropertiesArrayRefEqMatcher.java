package org.test4j.hamcrest.matcher.property;


import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.test4j.hamcrest.matcher.modes.EqMode;
import org.test4j.tools.IKit;
import org.test4j.tools.commons.StringHelper;

/**
 * 集合（数组）中对象的多个属性值集合(二维数组)，反射值与期望值相等
 *
 * @author darui.wudr
 */
public class PropertiesArrayRefEqMatcher extends BaseMatcher<Object> {
    private final String[] properties;
    private final Object[][] expected;

    ReflectionEqualMatcher matcher;

    public PropertiesArrayRefEqMatcher(String[] properties, Object[][] expected, EqMode... modes) {
        if (properties == null || properties.length == 0) {
            throw new RuntimeException("properties list can't be null!");
        }
        this.properties = properties;
        this.expected = expected;
        this.matcher = new ReflectionEqualMatcher(expected, modes);
    }

    @Override
    public boolean matches(Object actual) {
        this.propertyValues = IKit.ognl.valueList(actual, this.properties);
        return matcher.matches(propertyValues);
    }

    private Object[][] propertyValues = null;

    @Override
    public void describeTo(Description description) {
        description.appendText("the propery[" + StringHelper.toJsonString(this.properties) + "] of object must match");

        description.appendText(String.format(",but actual value is:%s, not matched value[%s]", StringHelper
            .toJsonString(this.propertyValues), StringHelper.toJsonString(this.expected)));
    }
}