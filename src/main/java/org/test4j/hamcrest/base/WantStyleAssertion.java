package org.test4j.hamcrest.base;

import org.test4j.functions.SExecutor;
import org.test4j.hamcrest.iassert.AssertHelper;
import org.test4j.hamcrest.iassert.impl.*;
import org.test4j.hamcrest.iassert.intf.*;
import org.test4j.tools.commons.ArrayHelper;

import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

@SuppressWarnings({"all"})
public class WantStyleAssertion {
    /**
     * 字符串断言
     *
     * @param value 字符串变量
     * @return ignore
     */
    public IStringAssert string(String value) {
        return new StringAssert(value);
    }

    /**
     * 布尔变量断言
     *
     * @param value a boolean variable
     * @return ignore
     */
    public IBooleanAssert bool(Boolean value) {
        return new BooleanAssert(value);
    }

    /**
     * integer变量断言
     *
     * @param value a integer variable
     * @return ignore
     */
    public IIntegerAssert number(Integer value) {
        return new IntegerAssert(value);
    }

    /**
     * short变量断言
     *
     * @param value a short variable
     * @return ignore
     */
    public IShortAssert number(Short value) {
        return new ShortAssert(value);
    }

    /**
     * long变量断言
     *
     * @param value a long variable
     * @return ignore
     */
    public ILongAssert number(Long value) {
        return new LongAssert(value);
    }

    /**
     * double变量断言
     *
     * @param value a double variable
     * @return ignore
     */
    public IDoubleAssert number(Double value) {
        return new DoubleAssert(value);
    }

    /**
     * float变量断言
     *
     * @param value a float variable
     * @return ignore
     */
    public IFloatAssert number(Float value) {
        return new FloatAssert(value);
    }

    /**
     * bigdecimal断言
     *
     * @param decimal BigDecimal
     * @return ignore
     */
    public INumberAssert<BigDecimal, ?> number(BigDecimal decimal) {
        return new NumberAssert(decimal, NumberAssert.class, BigDecimal.class);
    }

    /**
     * bigInteger断言
     *
     * @param bigint BigInteger
     * @return ignore
     */
    public INumberAssert<BigInteger, ?> number(BigInteger bigint) {
        return new NumberAssert(bigint, NumberAssert.class, BigInteger.class);
    }

    /**
     * Byte断言
     *
     * @param byt Byte
     * @return ignore
     */
    public INumberAssert<Byte, ?> number(Byte byt) {
        return new NumberAssert(byt, NumberAssert.class, Byte.class);
    }

    /**
     * char变量断言
     *
     * @param value a character variable
     * @return ignore
     */
    public ICharacterAssert character(Character value) {
        return new CharacterAssert(value);
    }

    /**
     * byte变量断言
     *
     * @param value a byte variable
     * @return ignore
     */
    public IByteAssert bite(Byte value) {
        return new ByteAssert(value);
    }

    /**
     * 数组变量断言
     *
     * @param value a array variable
     * @return ignore
     */
    public <T> IArrayAssert array(T[] value) {
        return new ArrayAssert(value);
    }

    /**
     * 数组变量断言
     *
     * @param value a array variable
     * @return ignore
     */
    public <T> IArrayAssert list(T[] value) {
        return new ArrayAssert(value);
    }

    /**
     * 布尔值数组变量断言
     *
     * @param value a boolean array
     * @return ignore
     */
    public IArrayAssert list(boolean[] value) {
        return new ArrayAssert(ArrayHelper.toArray(value));
    }

    /**
     * byte数组变量断言
     *
     * @param value a byte array
     * @return ignore
     */
    public IArrayAssert list(byte[] value) {
        return new ArrayAssert(ArrayHelper.toArray(value));
    }

    /**
     * character数组变量断言
     *
     * @param value a character array
     * @return ignore
     */
    public IArrayAssert list(char[] value) {
        return new ArrayAssert(ArrayHelper.toArray(value));
    }

    /**
     * short数值类型数组变量断言
     *
     * @param value a short array
     * @return ignore
     */
    public IArrayAssert list(short[] value) {
        return new ArrayAssert(ArrayHelper.toArray(value));
    }

    /**
     * integer数值类型数组变量断言
     *
     * @param value a integer array
     * @return ignore
     */
    public IArrayAssert list(int[] value) {
        return new ArrayAssert(ArrayHelper.toArray(value));
    }

    /**
     * long数值类型数组变量断言
     *
     * @param value a long array
     * @return ignore
     */
    public IArrayAssert list(long[] value) {
        return new ArrayAssert(ArrayHelper.toArray(value));
    }

    /**
     * float数值类型数组变量断言
     *
     * @param value a float array
     * @return ignore
     */
    public IArrayAssert list(float[] value) {
        return new ArrayAssert(ArrayHelper.toArray(value));
    }

    /**
     * double数值类型数组变量断言
     *
     * @param value a double array
     * @return ignore
     */
    public IArrayAssert list(double[] value) {
        return new ArrayAssert(ArrayHelper.toArray(value));
    }

    /**
     * 布尔值数组变量断言
     *
     * @param value a boolean array
     * @return ignore
     */
    public IArrayAssert array(boolean[] value) {
        return new ArrayAssert(ArrayHelper.toArray(value));
    }

    /**
     * byte数组变量断言
     *
     * @param value a byte array
     * @return ignore
     */
    public IArrayAssert array(byte[] value) {
        return new ArrayAssert(ArrayHelper.toArray(value));
    }

    /**
     * character数组变量断言
     *
     * @param value a character array
     * @return ignore
     */
    public IArrayAssert array(char[] value) {
        return new ArrayAssert(ArrayHelper.toArray(value));
    }

    /**
     * short数值类型数组变量断言
     *
     * @param value a short array
     * @return ignore
     */
    public IArrayAssert array(short[] value) {
        return new ArrayAssert(ArrayHelper.toArray(value));
    }

    /**
     * integer数值类型数组变量断言
     *
     * @param value a integer array
     * @return ignore
     */
    public IArrayAssert array(int[] value) {
        return new ArrayAssert(ArrayHelper.toArray(value));
    }

    /**
     * long数值类型数组变量断言
     *
     * @param value a long array
     * @return ignore
     */
    public IArrayAssert array(long[] value) {
        return new ArrayAssert(ArrayHelper.toArray(value));
    }

    /**
     * float数值类型数组变量断言
     *
     * @param value a float array
     * @return ignore
     */
    public IArrayAssert array(float[] value) {
        return new ArrayAssert(ArrayHelper.toArray(value));
    }

    /**
     * double数值类型数组变量断言
     *
     * @param value a double array
     * @return ignore
     */
    public IArrayAssert array(double[] value) {
        return new ArrayAssert(ArrayHelper.toArray(value));
    }

    /**
     * map变量断言
     *
     * @param map a map argument
     * @return ignore
     */
    public IMapAssert map(Map map) {
        return new MapAssert(map);
    }

    /**
     * collection变量断言
     *
     * @param collection a collection argument
     * @return ignore
     */
    public ICollectionAssert list(Collection collection) {
        return new CollectionAssert(collection);
    }

    /**
     * 通用object对象断言
     *
     * @param bean a object argument
     * @return ignore
     */
    public IObjectAssert object(Object bean) {
        return new ObjectAssert(bean);
    }

    /**
     * 一个永远失败的断言
     */
    public void fail() {
        // assert true == false;
        this.bool(true).isEqualTo(false);
    }

    /**
     * 一个永远失败的断言
     *
     * @param message 失败后的提示信息
     */
    public void fail(String message) {
        // assert true == false : message;
        this.bool(true).isEqualTo(message, false);
    }

    /**
     * a file argument asserter
     *
     * @param filename name of file
     * @return ignore
     */
    public IFileAssert file(String filename) {
        File file = new File(filename.replace("file:", ""));
        return new FileAssert(file);
    }

    /**
     * 文件变量断言
     *
     * @param file file
     * @return ignore
     */
    public IFileAssert file(File file) {
        return new FileAssert(file);
    }

    /**
     * calendar变量断言
     *
     * @param cal Calendar
     * @return ignore
     */
    public IDateAssert<Calendar> calendar(Calendar cal) {
        return new DateAssert<>(cal, Calendar.class);
    }

    /**
     * 日期变量断言
     *
     * @param date Date
     * @return ignore
     */
    public IDateAssert<Date> date(Date date) {
        return new DateAssert<>(date, Date.class);
    }

    /**
     * 期望执行异常
     *
     * @param executor 具体执行动作
     * @param eKlass   期望执行抛出的异常列表
     * @return ignore
     */
    public IStringAssert exception(SExecutor executor, Class... eKlass) {
        return AssertHelper.exception(executor, eKlass);
    }
}