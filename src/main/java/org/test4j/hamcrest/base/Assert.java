package org.test4j.hamcrest.base;

/**
 * 简单断言
 *
 * @author wudarui
 */
@SuppressWarnings({"unused"})
public class Assert {
    public static void notNull(Object obj, String message, Object... args) {
        if (obj == null) {
            throw new AssertionError(String.format(message, args));
        }
    }
}