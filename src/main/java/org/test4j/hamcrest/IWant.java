package org.test4j.hamcrest;

import org.test4j.hamcrest.base.TheStyleAssertion;
import org.test4j.hamcrest.base.WantStyleAssertion;

public interface IWant {
    WantStyleAssertion want = new WantStyleAssertion();

    TheStyleAssertion the = new TheStyleAssertion();
}