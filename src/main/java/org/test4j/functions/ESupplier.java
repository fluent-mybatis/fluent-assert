package org.test4j.functions;

import java.io.Serializable;

@SuppressWarnings({"unused"})
@FunctionalInterface
public interface ESupplier<T> extends Serializable {
    /**
     * 方法可以抛出异常的Supplier
     *
     * @return ignore
     * @throws Throwable
     */
    T get() throws Throwable;
}