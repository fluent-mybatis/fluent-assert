package org.test4j.functions;

import java.io.Serializable;

@SuppressWarnings({"unused"})
@FunctionalInterface
public interface EConsumer<T> extends Serializable {
    /**
     * 带异常的Consumer
     *
     * @param t object
     * @throws Throwable Exception
     */
    void accept(T t) throws Throwable;
}