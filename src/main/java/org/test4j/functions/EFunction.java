package org.test4j.functions;

import java.io.Serializable;

@SuppressWarnings({"unused"})
@FunctionalInterface
public interface EFunction<T, R> extends Serializable {
    /**
     * 方法可以抛出异常的Function
     *
     * @param t object
     * @return ignore
     */
    R apply(T t) throws Throwable;
}