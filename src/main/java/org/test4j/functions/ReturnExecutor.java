package org.test4j.functions;

import java.io.Serializable;

/**
 * ReturnExecutor 有返回值的执行
 *
 * @author wudarui
 */
@SuppressWarnings({"unused"})
@FunctionalInterface
public interface ReturnExecutor extends Serializable {
    /**
     * 执行动作
     *
     * @return 返回值
     */
    Object doIt() throws Throwable;

    /**
     * 封装SExecutor为ReturnExecutor
     *
     * @param executor SExecutor
     * @return ignore
     */
    static ReturnExecutor wrap(SExecutor executor) {
        return () -> {
            executor.doIt();
            return null;
        };
    }
}