package org.test4j.exception;

import lombok.Getter;

/**
 * 携带额外消息的断言异常
 *
 * @author darui.wu 2020/4/14 4:51 下午
 */
@Getter
public class ExtraMessageError extends Error {
    private final String extra;

    public ExtraMessageError(String message, Throwable cause) {
        super(message + cause.getMessage(), cause);
        this.extra = message;
    }
}