package org.test4j.exception;

import org.test4j.tools.commons.StringHelper;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.UndeclaredThrowableException;
import java.util.List;

/**
 * 异常包装器
 *
 * @author darui.wudr
 */
@SuppressWarnings({"unused"})
public class Exceptions {
    /**
     * 返回反射代理的本源异常
     *
     * @param e Exception
     * @return ignore
     */
    public static RuntimeException getUndeclaredThrowableExceptionCaused(Throwable e) {
        if (!(e instanceof UndeclaredThrowableException)) {
            return wrapWithRuntimeException(e);
        }
        Throwable e1 = ((UndeclaredThrowableException) e).getUndeclaredThrowable();
        if (!(e1 instanceof InvocationTargetException)) {
            return wrapWithRuntimeException(e1);
        }
        Throwable e2 = ((InvocationTargetException) e1).getTargetException();
        return wrapWithRuntimeException(e2);
    }

    public static RuntimeException wrapWithRuntimeException(Throwable e) {
        if (e instanceof RuntimeException) {
            return (RuntimeException) e;
        } else {
            return new RuntimeException(e);
        }
    }

    public static String toString(Throwable e) {
        return StringHelper.toString(e);
    }

    public static String toString(Throwable e, List<String> filter) {
        return StringHelper.toString(e, filter);
    }
}